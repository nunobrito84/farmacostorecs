﻿namespace FarmacoStore.Dialog
{
    partial class CredentialsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ti_api_key = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblBookie = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ti_api_username = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ti_pharma_name = new System.Windows.Forms.TextBox();
            this.ti_db_conn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_store_type = new System.Windows.Forms.ComboBox();
            this.cb_api_version = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_bs_name = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ti_base_url = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ti_emailTo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Base Url";
            this.toolTip1.SetToolTip(this.label1, "https://farmaciax.pt/\r\nor\r\nhttps://farmaciax.pt/demo/");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 330);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Api Key";
            // 
            // ti_api_key
            // 
            this.ti_api_key.Location = new System.Drawing.Point(122, 325);
            this.ti_api_key.Name = "ti_api_key";
            this.ti_api_key.Size = new System.Drawing.Size(266, 22);
            this.ti_api_key.TabIndex = 3;
            this.ti_api_key.TextChanged += new System.EventHandler(this.reevaluateSaveBtn);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(307, 485);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 33);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 485);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblBookie
            // 
            this.lblBookie.AutoSize = true;
            this.lblBookie.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBookie.Location = new System.Drawing.Point(12, 42);
            this.lblBookie.Name = "lblBookie";
            this.lblBookie.Size = new System.Drawing.Size(176, 29);
            this.lblBookie.TabIndex = 6;
            this.lblBookie.Text = "Configurações";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(260, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Farmaco Store";
            // 
            // ti_api_username
            // 
            this.ti_api_username.Location = new System.Drawing.Point(122, 291);
            this.ti_api_username.Name = "ti_api_username";
            this.ti_api_username.Size = new System.Drawing.Size(266, 22);
            this.ti_api_username.TabIndex = 24;
            this.toolTip1.SetToolTip(this.ti_api_username, "example: farmaciaxpto.pt");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Pharma Name";
            this.toolTip1.SetToolTip(this.label6, "Farmácia X");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "DB Conn";
            this.toolTip1.SetToolTip(this.label4, "1.1.1.1:1521/ORCL\r\nor\r\nORCL");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Api Username";
            this.toolTip1.SetToolTip(this.label10, "Default");
            // 
            // ti_pharma_name
            // 
            this.ti_pharma_name.Location = new System.Drawing.Point(123, 91);
            this.ti_pharma_name.Name = "ti_pharma_name";
            this.ti_pharma_name.Size = new System.Drawing.Size(266, 22);
            this.ti_pharma_name.TabIndex = 13;
            // 
            // ti_db_conn
            // 
            this.ti_db_conn.Location = new System.Drawing.Point(123, 125);
            this.ti_db_conn.Name = "ti_db_conn";
            this.ti_db_conn.Size = new System.Drawing.Size(266, 22);
            this.ti_db_conn.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 365);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Api Version";
            // 
            // cb_store_type
            // 
            this.cb_store_type.FormattingEnabled = true;
            this.cb_store_type.Items.AddRange(new object[] {
            "Opencart",
            "Shopify"});
            this.cb_store_type.Location = new System.Drawing.Point(123, 216);
            this.cb_store_type.Name = "cb_store_type";
            this.cb_store_type.Size = new System.Drawing.Size(266, 24);
            this.cb_store_type.TabIndex = 17;
            // 
            // cb_api_version
            // 
            this.cb_api_version.FormattingEnabled = true;
            this.cb_api_version.Items.AddRange(new object[] {
            "2020-04"});
            this.cb_api_version.Location = new System.Drawing.Point(123, 362);
            this.cb_api_version.Name = "cb_api_version";
            this.cb_api_version.Size = new System.Drawing.Size(266, 24);
            this.cb_api_version.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Store Type";
            // 
            // cb_bs_name
            // 
            this.cb_bs_name.FormattingEnabled = true;
            this.cb_bs_name.Items.AddRange(new object[] {
            "Sifarma 2000"});
            this.cb_bs_name.Location = new System.Drawing.Point(122, 163);
            this.cb_bs_name.Name = "cb_bs_name";
            this.cb_bs_name.Size = new System.Drawing.Size(266, 24);
            this.cb_bs_name.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Bs Name";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(-26, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(465, 3);
            this.label9.TabIndex = 22;
            // 
            // ti_base_url
            // 
            this.ti_base_url.Location = new System.Drawing.Point(122, 255);
            this.ti_base_url.Name = "ti_base_url";
            this.ti_base_url.Size = new System.Drawing.Size(266, 22);
            this.ti_base_url.TabIndex = 2;
            this.toolTip1.SetToolTip(this.ti_base_url, "example: farmaciaxpto.pt");
            this.ti_base_url.TextChanged += new System.EventHandler(this.reevaluateSaveBtn);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(-26, 406);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(465, 3);
            this.label11.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 421);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 17);
            this.label12.TabIndex = 26;
            this.label12.Text = "Email to";
            this.toolTip1.SetToolTip(this.label12, "1.1.1.1:1521/ORCL\r\nor\r\nORCL");
            // 
            // ti_emailTo
            // 
            this.ti_emailTo.Location = new System.Drawing.Point(122, 418);
            this.ti_emailTo.Name = "ti_emailTo";
            this.ti_emailTo.Size = new System.Drawing.Size(266, 22);
            this.ti_emailTo.TabIndex = 27;
            // 
            // CredentialsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(401, 530);
            this.Controls.Add(this.ti_emailTo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ti_api_username);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cb_bs_name);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cb_api_version);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cb_store_type);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ti_db_conn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ti_pharma_name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblBookie);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ti_api_key);
            this.Controls.Add(this.ti_base_url);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CredentialsDlg";
            this.Text = "ConfigBookieDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ti_api_key;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblBookie;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox ti_pharma_name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ti_db_conn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cb_store_type;
        private System.Windows.Forms.ComboBox cb_api_version;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_bs_name;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ti_api_username;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ti_base_url;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ti_emailTo;
    }
}