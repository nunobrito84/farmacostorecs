﻿using FarmacoStore.Models;
using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmacoStore.Dialog
{
    /// <summary>
    /// Interaction logic for CategoriesCreateDlg.xaml
    /// </summary>
    public partial class CategoriesCreateDlg : MetroWindow, INotifyPropertyChanged
    {
        public bool processing { get; set; } = false;
        public string loading_text { get; set; } = "";
        private ObservableCollection<ICategory> _categories;

        private ObservableCollection<ICategory> _seedCategories;
        private ObservableCollection<ICategory> _storeCategories;

        private void setCategoryPropertyListener(ICategory category) {
            category.PropertyChanged += onObjectPropertyChanged;
            if (category.children != null) {
                foreach (ICategory cat in category.children)
                    setCategoryPropertyListener(cat);//recursive
            }
        }

        public ObservableCollection<ICategory> categories
        {
            get { return _categories; }
            set
            {
                _categories = value;
                if (_categories != null)
                    foreach (ICategory category in _categories)
                    {
                        category.Initialize();
                        setCategoryPropertyListener(category);
                    }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        private void onObjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
                this.NotifyPropertyChanged("categoriesSelectedCount");
        }
        private string _storeType, _storeUrl, _storeAPI;
        public CategoriesCreateDlg()
        {
            InitializeComponent();
            this.DataContext = this;
            ServerCredentials credentials = new ServerCredentials();
            _storeType = credentials.get_store_type();//To be used several times
            _storeUrl = credentials.get_store_url();
            _storeAPI = credentials.get_api_key();
            //Seed categories
            _seedCategories = SeedsManager.Instance.loadSeedCategories(_storeType, writeLog);
            //Store categories

            IStore store = StoreFactory.Instance(_storeType, _storeUrl, _storeAPI);
            _storeCategories = store.getCategories();

            categories = mergeCategories(_seedCategories, _storeCategories);
            //Merge store categories to seed categories
            Console.WriteLine("a");
        }
        private ObservableCollection<ICategory> mergeCategories(ObservableCollection<ICategory> seedCategories, ObservableCollection<ICategory> storeCategories) {
            foreach(ICategory cat in storeCategories) {
                ICategory foundCategory = findCategory(seedCategories, cat.category_id);
                if (foundCategory == null) {
                    if (cat.parent_id == 0) //Add root category
                        seedCategories.Add(cat);
                } else {
                    foundCategory.SetSelectionEnabled(false);
                    /*
                    ICategory foundParent = findCategory(categories, cat.parent_id);
                    if (foundParent != null) {
                        cat.IsSelectionEnabled = false;
                        foundParent.children.Add(cat);
                    }
                    */
                }
            }
            return seedCategories;
        }
        private ICategory findCategory(ObservableCollection<ICategory> list, int categoryIdToFind) {
            foreach (ICategory cat in list) {
                if (cat.category_id == categoryIdToFind)
                    return cat;
                ICategory childresult = findCategory(cat.children, categoryIdToFind);
                if (childresult != null)
                    return childresult;
            }
            return null;
        }

        private void onStartProcessing(object sender, RoutedEventArgs e)
        {
            processing = true;
            Task.Run(() => saveCategories());
        }
        private void saveCategories() {
            try {
                foreach (ICategory cat in categories)
                    insertCategory(cat);
            } catch { //Ignore here. the message is handled internally

            }
            processing = false;
        }

        private void insertCategory(ICategory category) { //Recursive
            if(category.IsSelected) {
                try {
                    StoreFactory.Instance(_storeType, _storeUrl, _storeAPI).insertCategory(category);
                    writeLog($"Category insert success: {category.category_id}: {category.name} | slug: {category.seo_keyword}");
                    category.ItemBackColor = System.Windows.Media.Brushes.LightGreen;
                    category.SetIsChecked(false, false, false);
                    category.SetSelectionEnabled(false, false, false);
                } catch (Exception e) {
                    category.ItemBackColor = System.Windows.Media.Brushes.LightSalmon;
                    writeLog(e.Message, true);
                    throw e;
                }
            }
            foreach (ICategory child in category.children)
                insertCategory(child);
        }

        private String logStr = "";
        private void writeLog(String str, bool isError = false)
        {
            if (!CheckAccess())
            {
                // On a different thread
                Dispatcher.Invoke(() => writeLog(str, isError));
                return;
            }

            const int STR_MAX_LEN = 100000;
            if (logStr.Length > STR_MAX_LEN)
                logStr = "-- CUTTED -- " + Environment.NewLine + logStr.Substring(STR_MAX_LEN / 2);
            else
                logStr += $"[{DateTime.Now.ToString("HH:mm:ss")}]: {str}" + Environment.NewLine;
            EventLog.WriteEntry(App.windows_log_entry, str, EventLogEntryType.SuccessAudit);
            log.Text += $"[{DateTime.Now.ToString("HH:mm:ss")}]: {str}" + Environment.NewLine;
        }
        private LogView logView;
        public void showLog(object sender, RoutedEventArgs e)
        {
            if (logView != null && logView.Visible)
                logView.Close();
            logView = new LogView(this.logStr);
            logView.StartPosition = FormStartPosition.CenterParent;
            logView.ShowDialog();
        }

        private void selectAllItems(object sender, RoutedEventArgs e) {
            foreach(ICategory cat in categories) {
                if (cat.IsSelectionEnabled)
                    cat.SetIsChecked(true, true, false);
            }
        }
        private void deselectAllItems(object sender, RoutedEventArgs e) {
            foreach (ICategory cat in categories) {
                if (cat.IsSelectionEnabled)
                    cat.SetIsChecked(false, true, false);
            }
        }
        

        private int countSelected(ICategory category) {
            int cnt2 = category.IsSelected == true ? 1 : 0;
            if (category.children != null)
                foreach (ICategory childCategory in category.children)
                    cnt2 += countSelected(childCategory);
            return cnt2;
        }
        public int categoriesSelectedCount { 
            get
            {
                if (categories == null)
                    return 0;
                int cnt = 0;
                foreach (ICategory category in categories)
                    cnt += countSelected(category);
                return cnt;
            }
        }
    }

}
