﻿using FarmacoStore.Controller;
using FarmacoStore.Dialog;
using FarmacoStore.Models;
using FarmacoStore.Models.BillingSystem;
using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using MahApps.Metro.Controls;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Utils.ListVirtualization;

namespace FarmacoStore.Dialog {
    /// <summary>
    /// Interaction logic for ProductsUpdateDlg.xaml
    /// </summary>
    public partial class ProductsUpdateDlg : MetroWindow {
        private LogView logView;
        
        private ServerCredentials serverCredentials = new ServerCredentials();
        String baseUrl, apiKey, dbConn, billingSystemType, storeType;
        public float selectedTotal = 0;

        public ProductsUpdateDlg() {
            EventLog.WriteEntry(App.windows_log_entry, "ProductsUpdateDlg INIT", EventLogEntryType.SuccessAudit);
            InitializeComponent();
            this.DataContext = UpdateProcessor.Instance;
            UpdateProcessor.Instance.PropertyChanged += Instance_PropertyChanged;
        }

        private const int MIN_NAME_FILTER_LEN = 5;
        private bool MyCustomFilter(object item) {
            if (String.IsNullOrEmpty(ti_nameFilter.Text) || ti_nameFilter.Text.Length < MIN_NAME_FILTER_LEN)
                return true;
            else {
                if ((item as IDbProduct).name.IndexOf(ti_nameFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
            }
            return false; 
        }

        private void Instance_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (e.PropertyName == "productsList") {
                //applyContext(new AsyncVirtualizingCollection<IDbProduct>(new ProductProvider(UpdateProcessor.Instance.productsList.ToList()), 100));
                applyContext(new VirtualizingCollection<IDbProduct>(new ProductProvider(UpdateProcessor.Instance.productsList.ToList())));
            }
        }

        void applyContext(VirtualizingCollection<IDbProduct> newContext) {
            if (!CheckAccess()) {
                // On a different thread
                Dispatcher.Invoke(() => applyContext(newContext));
                return;
            }
            myList.DataContext = newContext;

        }
        private void ti_nameFilter_TextChanged(object sender, TextChangedEventArgs e) {
            ICollectionView view = CollectionViewSource.GetDefaultView(myList.ItemsSource);
            if (!String.IsNullOrEmpty(ti_nameFilter.Text) && ti_nameFilter.Text.Length >= MIN_NAME_FILTER_LEN)
                view.Filter = MyCustomFilter;
            else view.Filter = null;

            Dispatcher.BeginInvoke( //call on Background thread
              DispatcherPriority.Background,
              new Action(() => {
                  view.Refresh();
              }));
        }

        void onItemFocus(object sender, UpdateProcessor.ItemFocusEventArgs args)  
        {
            Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() => {
                  if (myList.Items.Count > 0) {
                      myList.SelectedItem = args.Product;
                      myList.ScrollIntoView(myList.Items[0]);
                      myList.ScrollIntoView(myList.SelectedItem);
                      Brush prevColor = args.Product.ItemBackColor;
                      args.Product.ItemBackColor = System.Windows.Media.Brushes.LightGoldenrodYellow;
                      Task.Delay(300).ContinueWith(t => args.Product.ItemBackColor = prevColor);
                  }
              }));
        }
        private void onLoad(object sender, RoutedEventArgs e) {
            UpdateProcessor.Instance.ItemFocus += onItemFocus;
            try {
                baseUrl = serverCredentials.get_store_url();
                apiKey = serverCredentials.get_api_key();
                dbConn = serverCredentials.get_db_conn();
                billingSystemType = serverCredentials.get_bs_name();
                storeType = serverCredentials.get_store_type();
            } catch {
                //configuraçõesToolStripMenuItem_Click(null, null);
                return;
            }
            try {
               // #if (DEBUG)
                  onStartProcessing(null, null);
               // #endif                
            } catch {

            }
        }

        private String logStr = "";
        void writeLog(String str, bool isError = false) {
            if (!CheckAccess()) {
                // On a different thread
                Dispatcher.Invoke(() => writeLog(str, isError));
                return;
            }

            const int STR_MAX_LEN = 100000;
            if (logStr.Length > STR_MAX_LEN)
                logStr = "-- CUTTED -- " + Environment.NewLine + logStr.Substring(STR_MAX_LEN / 2);
            else
                logStr += $"[{DateTime.Now.ToString("HH:mm:ss")}]: {str}" + Environment.NewLine;
            EventLog.WriteEntry(App.windows_log_entry, str, EventLogEntryType.SuccessAudit);
            log.Text += $"[{DateTime.Now.ToString("HH:mm:ss")}]: {str}" + Environment.NewLine;
            log.ScrollToEnd();
        }

        private async Task onStartProcessing(object sender, RoutedEventArgs e)
        {
            try
            {
                await Task.Run(() =>
                {
                    UpdateProcessor.Instance.process(true, writeLog, this);
                });
            }
            catch(Exception error)
            {
                writeLog($"{error.Message}");
            }
        }

        private void performUpdate_Click(object sender, RoutedEventArgs e) {
            Task.Run(() =>
            {
                UpdateProcessor.Instance.update(true, writeLog, this);
            });
        }

        public void onSelectAll(object sender, RoutedEventArgs e) {
            foreach(IDbProduct product in UpdateProcessor.Instance.productsList) {
                if (product.IsSelectionEnabled && !product.IsInWebStore)
                    product.IsSelected = true;
            }
        }
        public void showLog(object sender, RoutedEventArgs e) {
            if (logView != null && logView.Visible)
                logView.Close();
            logView = new LogView(this.logStr);
            logView.StartPosition = FormStartPosition.CenterParent;
            logView.ShowDialog();
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e) {
            Console.Write("Mouse down");
            System.Drawing.Image image = ((sender as Image).DataContext as IDbProduct).ImageFile as System.Drawing.Image;

            // Winforms Image we want to get the WPF Image from...
            var bitmap = new System.Windows.Media.Imaging.BitmapImage();
            bitmap.BeginInit();
            MemoryStream memoryStream = new MemoryStream();
            // Save to a memory stream...
            image.Save(memoryStream, ImageFormat.Jpeg);
            // Rewind the stream...
            memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
            bitmap.StreamSource = memoryStream;
            bitmap.EndInit();

            ImagePopupContent.Source = bitmap;
            ImagePopup.IsOpen = true; 
        }

        private void CloseImagePopUpBtn_Click(object sender, MouseButtonEventArgs e) {
            ImagePopup.IsOpen = false;
        }

        private void reloadAllClick(object sender, RoutedEventArgs e) {
            Task.Run(() => {
                UpdateProcessor.Instance.process(true, writeLog, this);
            });
        }

        private ObservableCollection<ICategory> _storeTreeCategories;
        private IDbProduct selectedCategoryProduct;
        private void onAddCategoryClick(object sender, RoutedEventArgs e) {
            IDbProduct product = ((sender as System.Windows.Controls.Button).DataContext) as IDbProduct;
            Console.Write("a");
            if (_storeTreeCategories == null)
                buildCategoriesTree();

            selectedCategoryProduct = product;
            //Clear all
            foreach (ICategory category in _storeTreeCategories) {
                category.SetIsChecked(false, true, false);
                category.SetSelectionEnabled(true, true, false);
            }
            //Select previous
            foreach (var storeCategory in product.categories) {
                ICategory foundCategory = CategoryBase.findCategory(_storeTreeCategories, storeCategory.category_id);
                if(foundCategory != null)
                    foundCategory.IsSelectionEnabled = false;//Dont allow selection on category already in database
            }
            trvCategories.ItemsSource = _storeTreeCategories;
            CategorySelectorPopup.IsOpen = true;
        } 

        private void CloseCategoriesSelectBtn_Click(object sender, MouseButtonEventArgs e) {
            CategorySelectorPopup.IsOpen = false;
        }

        private void OnCheckboxChange(object sender, RoutedEventArgs e) {
            bool? isChecked = (sender as System.Windows.Controls.CheckBox).IsChecked;
            ICategory category = (sender as System.Windows.Controls.CheckBox).DataContext as ICategory;
            if (isChecked == true) //Add to list
                selectedCategoryProduct.addCategoryToUpdateData(category);
            else //Remove from list
                selectedCategoryProduct.removeCategoryFromUpdateData(category);
        }

        private void buildCategoriesTree() {
            writeLog("Loading seed categories");
            _storeTreeCategories = new ObservableCollection<ICategory>();
            foreach(ICategory category in UpdateProcessor.Instance.storeCategories) {//it is already sorted
                Console.WriteLine("Category: " + category.name);
                if (category.parent_id == 0) {
                    _storeTreeCategories.Add(StoreFactory.StoreCategory(storeType, category.category_id, category.parent_id, category.name, new ObservableCollection<ICategory>()));
                } else {
                    ICategory foundCat = CategoryBase.findCategory(_storeTreeCategories, category.parent_id);
                    if (foundCat != null)
                        foundCat.children.Add(StoreFactory.StoreCategory(storeType, category.category_id, category.parent_id, category.name, new ObservableCollection<ICategory>()));
                }
            }
        }
        public void showCategories(object sender, RoutedEventArgs e)
        {
            CategoriesCreateDlg modalWindow = new CategoriesCreateDlg();
            modalWindow.ShowDialog();
            Task.Run(() => {
                UpdateProcessor.Instance.loadCategories();
            });
        }



        public void showBrands(object sender, RoutedEventArgs e) {
            BrandsEvaluationDlg modalWindow = new BrandsEvaluationDlg();
            modalWindow.ShowDialog();
        }
        public void showCampaigns(object sender, RoutedEventArgs e) {
            CampaignsEditorDlg modalWindow = new CampaignsEditorDlg();
            modalWindow.ShowDialog();
        }

        public void searchImages(object sender, RoutedEventArgs e) {
            Task.Run(() => {
                Dispatcher.Invoke(() => UpdateProcessor.Instance.loadImages(writeLog, chk_replaceImages.IsChecked.Value, this));
            });
        }

        private async void loadDescriptions(object sender, RoutedEventArgs e) {
            // Set the file dialog to filter for graphics files.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "CSV (*.csv)|*.csv";
            // Allow the user to select multiple files.
            openFileDialog1.Multiselect = true;
            openFileDialog1.Title = "My Image Browser";
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK) {
                await Task.Run(() => {
                    UpdateProcessor.Instance.loadDescriptions(openFileDialog1.FileNames, writeLog, this);
                });

            }
        }
    }
}
