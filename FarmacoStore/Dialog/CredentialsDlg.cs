﻿using FarmacoStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmacoStore.Dialog
{
    public partial class CredentialsDlg : Form
    {
        private ServerCredentials serverData = new ServerCredentials();

        public CredentialsDlg(ServerCredentials serverData)
        {
            this.serverData = serverData;
            InitializeComponent();
            try { ti_base_url.Text = serverData.get_store_url(); } catch { }
            try { ti_api_username.Text = serverData.get_api_username(); } catch { }
            try { ti_api_key.Text = serverData.get_api_key(); } catch { }
            try { ti_pharma_name.Text = serverData.get_pharma_name(); } catch { }
            try { ti_db_conn.Text = serverData.get_db_conn(); } catch { }
            try { cb_bs_name.SelectedIndex = cb_bs_name.FindStringExact(serverData.get_bs_name()); } catch { }
            try { cb_api_version.SelectedIndex = cb_api_version.FindStringExact(serverData.get_api_version()); } catch { }
            try { cb_store_type.SelectedIndex = cb_store_type.FindStringExact(serverData.get_store_type()); } catch { }
            try { ti_emailTo.Text = serverData.get_email_to(); } catch { }

            reevaluateSaveBtn(null, null);
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            serverData.set_store_url(ti_base_url.Text);
            serverData.set_api_username(ti_api_username.Text);
            serverData.set_api_key(ti_api_key.Text);
            serverData.set_pharma_name(ti_pharma_name.Text);
            serverData.set_db_conn(ti_db_conn.Text);
            serverData.set_bs_name(cb_bs_name.SelectedItem.ToString());
            serverData.set_api_version(cb_api_version.SelectedItem.ToString());
            serverData.set_store_type(cb_store_type.SelectedItem.ToString());
            serverData.set_email_to(ti_emailTo.Text);

            this.Close();
            //Restart App!
            Application.Restart();
            Environment.Exit(0);
        }

        private void reevaluateSaveBtn(object sender, EventArgs e)
        {
            btnSave.Enabled = ti_api_key.Text.Length > 0 && ti_base_url.Text.Length > 0 && ti_pharma_name.Text.Length > 0 && ti_db_conn.Text.Length > 0;
        }
    }
}
