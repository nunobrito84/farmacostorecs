﻿using FarmacoStore.Controller;
using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Seeds;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmacoStore.Dialog {
    /// <summary>
    /// Interaction logic for BrandsEvaluationDlg.xaml
    /// </summary>
    public partial class BrandsEvaluationDlg : MetroWindow {
        public BrandsEvaluationDlg() {
            InitializeComponent();

            var groupedBrandsList = UpdateProcessor.Instance.productsList
                .GroupBy(u => u.brand?.brand_id)
                .Select(
                    item => new {
                        BrandCode = item.Key,
                        BrandName = item.Min(x => x.brand?.name ?? "Desconhecida"),
                        WithStock = item.Count(x => x.stk_farmacia > 0),
                        WithoutStock = item.Count(x => x.stk_farmacia <= 0),
                        StockCount = item.Sum(x => x.stk_farmacia > 0 ? x.stk_farmacia : 0),
                        HasImage = item.Max(x => BrandSeedList.Instance.brands.FirstOrDefault(o => o.brand_id == x.brand?.brand_id)?.image != null),
                        image = item.Max(x => BrandSeedList.Instance.brands.FirstOrDefault(o => o.brand_id == x.brand?.brand_id)?.image)
                    }
                )
                .OrderByDescending(x => x.StockCount)
                .ToList();
            myGrid.ItemsSource = groupedBrandsList;
            Console.WriteLine("a");
        }
        private T GetValueFromAnonymousType<T>(object dataitem, string itemkey) {
            System.Type type = dataitem.GetType();
            T itemvalue = (T)type.GetProperty(itemkey).GetValue(dataitem, null);
            return itemvalue;
        }
        private void Image_MouseDown(object sender, MouseButtonEventArgs e) {
            //System.Drawing.Bitmap image = ((sender as System.Windows.Controls.Image).DataContext as BrandSeed).image as System.Drawing.Bitmap;

            System.Drawing.Bitmap image = GetValueFromAnonymousType<System.Drawing.Bitmap>((sender as System.Windows.Controls.Image).DataContext, "image");

            // Winforms Image we want to get the WPF Image from...
            var bitmap = new System.Windows.Media.Imaging.BitmapImage();
            bitmap.BeginInit();
            MemoryStream memoryStream = new MemoryStream();
            // Save to a memory stream...
            image.Save(memoryStream, ImageFormat.Png);
            // Rewind the stream...
            memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
            bitmap.StreamSource = memoryStream;
            bitmap.EndInit();

            ImagePopupContent.Source = bitmap;
            ImagePopup.IsOpen = true;
        }

        private void CloseImagePopUpBtn_Click(object sender, MouseButtonEventArgs e) {
            ImagePopup.IsOpen = false;
        }
    }


}
