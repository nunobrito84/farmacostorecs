﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmacoStore.Dialog
{
    public partial class LogView : Form
    {
        public LogView(String logStr)
        {
            InitializeComponent();
            this.ti_log.Text = logStr;
        }
    }
}
