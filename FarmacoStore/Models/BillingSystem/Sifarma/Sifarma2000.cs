﻿using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Store;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.BillingSystem.Sifarma
{
    class Sifarma2000 : BillingSystemBase, IBillingSystem
    {
        public string BsName { get { return "Sifarma 2000"; } }
        private const string USERNAME = "EXPORTA";
        private const string PASSWORD = "EXPORTA";
        private IStore _store;
        public Sifarma2000(string connectionStr) : base(connectionStr)
        {
            ServerCredentials serverCredentials = new ServerCredentials();
            string _storeType = serverCredentials.get_store_type();//To be used several times
            string _storeUrl = serverCredentials.get_store_url();
            string _storeAPI = serverCredentials.get_api_key();
            _store = StoreFactory.Instance(_storeType, _storeUrl, _storeAPI);
        }
        public IDbProduct ProductClass { get { return new DbProduct(); } }
        public List<IDbProduct> getProducts(logFunc log)
        {
            List<IDbProduct> productsList = new List<IDbProduct>();
            OracleConnection oracleConnection = new OracleConnection();
            // create connection string using builder
            OracleConnectionStringBuilder ocsb = new OracleConnectionStringBuilder();
            ocsb.UserID = "EXPORTA";
            ocsb.Password = "EXPORTA";
            ocsb.DataSource = connectionStr;
            // connect
            oracleConnection.ConnectionString = ocsb.ConnectionString;
            oracleConnection.Open();
            log("DB Connection established (" + oracleConnection.ServerVersion + ")");

            DBProperties dbProperties = new DBProperties(0);
            OracleCommand command = oracleConnection.CreateCommand();
            command.CommandText = "SELECT count(*) as VIEWEXISTS FROM user_views WHERE view_name = 'FCO_VERSION'";
            OracleDataReader reader = command.ExecuteReader();
            if (reader.Read())
            { //There is a view
                if (reader.GetInt32(0) == 1)
                {
                    command = oracleConnection.CreateCommand();
                    command.CommandText = "SELECT VERSION FROM FCO_VERSION";
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        dbProperties.version = float.Parse(reader.GetString(0).Replace('.', ','));
                        log($"DB Views version FCO: v{dbProperties.version}");
                    }
                }
                else
                {
                    log($"Using Default Views: v{dbProperties.version}");
                }
            }
            else throw new Exception("Views analysis returned empty... no Fco views?");
            //Get the sales...
            command = oracleConnection.CreateCommand();
#if (DEBUG)
            command.CommandText = $"SELECT * FROM FCO_PRODUTOS WHERE productcode >= '2000000' AND productcode < '2200000' ORDER BY PRODUCTCODE ASC";
            command.CommandText = $"SELECT * FROM FCO_PRODUTOS WHERE productcode >= '2000000' AND LOWER(name) LIKE '%lierac%' ORDER BY PRODUCTCODE ASC";
            //Chicco 4 - bioderma 7 - Neostrata 5
/*
            command.CommandText = @"SELECT * FROM FCO_PRODUTOS WHERE productcode IN (
                        --- no brand 
                        1393728,      
                        --- chicco 4 
                        6168971, 6168963, 6144584, 6135525,
                        --- bioderma 7 
                        6981860, 6981852, 6981837, 6980920, 6980912, 6980904, 6980896,
                        --- neostrata 5 
                        6847467, 6836643, 6831644, 6831636, 6821967) ORDER BY PRODUCTCODE ASC";
*/
            command.CommandText = $"SELECT * FROM FCO_PRODUTOS ORDER BY PRODUCTCODE ASC";
            //command.CommandText = $"SELECT * FROM FCO_PRODUTOS WHERE productcode IN ('6837831', '6274118') ORDER BY PRODUCTCODE ASC";//Um com 800x800 o outro apenas com o 288x390
#else
            command.CommandText = $"SELECT * FROM FCO_PRODUTOS WHERE productcode >= '2000000' AND pvp_lote > 0 AND pvp_lote < 1000 ORDER BY PRODUCTCODE ASC";
#endif
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                try {
                    int productcode = reader["productcode"] != DBNull.Value ? Convert.ToInt32(reader["productcode"]) : 0;
                    string name = reader["name"] != DBNull.Value ? (string)reader["name"] : null;
                    string fap = reader["fap"] != DBNull.Value ? (string)reader["fap"] : null;
                    string family = reader["family"] != DBNull.Value ? (string)reader["family"] : null;
                    string speciality = reader["speciality"] != DBNull.Value ? (string)reader["speciality"] : null;
                    string tipo_venda = reader["tipo_venda"] != DBNull.Value ? (string)reader["tipo_venda"] : null;
                    int iva_id = Convert.ToInt32(reader["iva_id"]);
                    decimal pvp_lote = reader["pvp_lote"] != DBNull.Value ? Convert.ToDecimal(reader["pvp_lote"]) : 1000000;
                    string gama = reader["gama"] != DBNull.Value ? (string)reader["gama"] : null;
                    decimal pcusmed_prod = reader["pcusmed_prod"] != DBNull.Value ? Convert.ToDecimal(reader["pcusmed_prod"]) : 1000000;
                    decimal pcus_prod = reader["pcus_prod"] != DBNull.Value ? Convert.ToDecimal(reader["pcus_prod"]) : 1000000;
                    int stk_farmacia = reader["stk_farmacia"] != DBNull.Value ? Convert.ToInt32(reader["stk_farmacia"]) : 0;
                    int stk_min = reader["stk_min"] != DBNull.Value ? Convert.ToInt32(reader["stk_min"]) : 0;
                    int stk_max = reader["stk_max"] != DBNull.Value ? Convert.ToInt32(reader["stk_max"]) : 0;
                    bool esgotado = (string)reader["esgotado"] == "N" ? false : true; IDbProduct dbProd = new DbProduct();
                    dbProd.build(_store,
                        productcode,
                        name,
                        fap,
                        family,
                        speciality,
                        tipo_venda,
                        iva_id,
                        pvp_lote,
                        gama,
                        pcusmed_prod,
                        pcus_prod,
                        stk_farmacia,
                        stk_min,
                        stk_max,
                        esgotado
                        );
                    productsList.Add((IDbProduct)dbProd);
                }
                catch(Exception e) {
                    /*
                    log($@"Error: 
                    productcode: {productcode} |
                    name: {name} |
                    fap: {fap} |
                    family: {family} |
                    speciality: {speciality} |
                    tipo_venda: {tipo_venda} |
                    iva_id: {iva_id} |
                    pvp_lote: {pvp_lote} |
                    gama: {gama} |
                    pcusmed_prod: {pcusmed_prod} |
                    pcus_prod: {pcus_prod} |
                    stk_farmacia: {stk_farmacia} |
                    stk_min: {stk_min} |
                    stk_max: {stk_max} |
                    esgotado: {esgotado} |
                    {e.Message} |
                    {e.StackTrace}");*/
                    
                    log($@"Error: 
                    productcode: {reader["productcode"]} |
                    name: {reader["name"]} |
                    fap: {reader["fap"]} |
                    family: {reader["family"]} |
                    speciality: {reader["speciality"]} |
                    tipo_venda: {reader["tipo_venda"]} |
                    iva_id: {reader["iva_id"]} |
                    pvp_lote: {reader["pvp_lote"]} |
                    gama: {reader["gama"]} |
                    pcusmed_prod: {reader["pcusmed_prod"]} |
                    pcus_prod: {reader["pcus_prod"]} |
                    stk_farmacia: {reader["stk_farmacia"]} |
                    stk_min: {reader["stk_min"]} |
                    stk_max: {reader["stk_max"]} |
                    esgotado: {reader["esgotado"]}
                    {e.Message} |
                    {e.StackTrace}");
                    
                    throw e;
                }

            }
            return productsList;
        }

    }
}
