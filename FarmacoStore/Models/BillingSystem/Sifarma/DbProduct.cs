﻿using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Opencart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.BillingSystem.Sifarma
{
    class DbProduct : DbProductBase, IDbProduct
    {
        //public string code { get; set; }
        public DbProduct() { }
        public DbProduct(string name) {
            this.name = name;
        }
    }
}
