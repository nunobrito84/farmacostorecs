﻿using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.BillingSystem.Sifarma;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.BillingSystem
{
    class BillingSystemFactory
    {
        public static IBillingSystem Instance(string bsName, string connStr) { 
            switch(bsName)
            {
                case "Sifarma 2000":
                    return new Sifarma2000(connStr); 
                default:
                    throw new Exception($"{bsName} instance not implemented.");
            }             
        }
    }
}
