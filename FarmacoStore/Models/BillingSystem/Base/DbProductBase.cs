﻿using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Models.Store.Opencart;
using FarmacoStore.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.BillingSystem.Interfaces
{
    class DbProductBase : INotifyPropertyChanged
    {

        public string ENGLISH_SHORT_CODE { get { return "en"; } }
        public string PORTUGUESE_SHORT_CODE { get { return "pt"; } }
        public bool ProductDataMismatch {
            get {
                if(!IsInWebStore)
                    return false;
                if (this.stk_farmacia != this.webstoreProduct.quantity ||
                    this.pvpNoVat != this.webstoreProduct.priceNoVat ||
                    this.webstoreTaxId != this.webstoreProduct.tax_class_id || 
                    this.brand != null && this.productUpdateData.manufacturer_id != null)
                    return true;
                if(productUpdateData != null) {
                    if (
                       (productUpdateData.name != null && this.webstoreProduct.name != productUpdateData.name) ||
                       (productUpdateData.description != null && this.webstoreProduct.description != productUpdateData.description) ||
                       (productUpdateData.meta_desc != null && this.webstoreProduct.meta_description != productUpdateData.meta_desc) ||
                       (productUpdateData.meta_title != null && this.webstoreProduct.meta_title != productUpdateData.meta_title) ||
                       (productUpdateData.meta_keyword != null && this.webstoreProduct.meta_keyword != productUpdateData.meta_keyword) ||
                       (productUpdateData.meta_tag != null && this.webstoreProduct.tags != productUpdateData.meta_tag)
                       )
                        return true;
                }
                return false;
            }
        }
        public string UpdateTooltip {
            get {
                if (IsInWebStore) {
                    List<String> tooltipReasons = new List<String>();
                    if (this.stk_farmacia != this.webstoreProduct.quantity)
                        tooltipReasons.Add($"• Quantidade: {this.stk_farmacia} vs {this.webstoreProduct.quantity}");
                    if (this.pvpNoVat != this.webstoreProduct.priceNoVat)
                        tooltipReasons.Add($"• Preço (s/iva): {this.pvpNoVat}€ vs {this.webstoreProduct.priceNoVat}€");
                    if (this.webstoreTaxId != this.webstoreProduct.tax_class_id)
                        tooltipReasons.Add($"• #IVA: {this.webstoreTaxId} vs {this.webstoreProduct.tax_class_id}");

                    if (productUpdateData.name != null && this.webstoreProduct.name != productUpdateData.name)
                        tooltipReasons.Add($"• #Nome: {this.webstoreProduct.name} vs {productUpdateData.name}");
                    if (productUpdateData.description != null && this.webstoreProduct.description != productUpdateData.description)
                        tooltipReasons.Add($"• #Descrição: {this.webstoreProduct.description} vs {productUpdateData.description}");
                    if (productUpdateData.meta_desc != null && this.webstoreProduct.meta_description != productUpdateData.meta_desc)
                        tooltipReasons.Add($"• #Metadescrição: {this.webstoreProduct.meta_description} vs {productUpdateData.meta_desc}");
                    if (productUpdateData.meta_title != null && this.webstoreProduct.meta_title != productUpdateData.meta_title)
                        tooltipReasons.Add($"• #Metatitle: {this.webstoreProduct.meta_title} vs {productUpdateData.meta_title}");
                    if (productUpdateData.meta_keyword != null && this.webstoreProduct.meta_keyword != productUpdateData.meta_keyword)
                        tooltipReasons.Add($"• #Metakeyword: {this.webstoreProduct.meta_keyword} vs {productUpdateData.meta_keyword}");
                    if (productUpdateData.meta_tag != null && this.webstoreProduct.tags != productUpdateData.meta_tag)
                        tooltipReasons.Add($"• #Metatag: {this.webstoreProduct.tags} vs {productUpdateData.meta_tag}");
                    if (productUpdateData.manufacturer_id != null && this.webstoreProduct.manufacturer_id.ToString() != productUpdateData.manufacturer_id)
                        tooltipReasons.Add($"• #Brand: {this.webstoreProduct.manufacturer_id} vs {productUpdateData.manufacturer_id}");

                    if (tooltipReasons.Count > 0)
                        tooltipReasons.Insert(0, "Farmácia vs Loja Web");
                    else tooltipReasons.Add("Nenhuma diferença detectada");
                    return String.Join("\n", tooltipReasons.ToArray());
                } else return "";
            }
        }
        public bool IsInWebStore {
            get { return _webstoreProduct != null; }
        }
        private IProduct _webstoreProduct;
        public IProduct webstoreProduct { get { return _webstoreProduct; }
            set {
                if (value == _webstoreProduct)
                    return;
                _webstoreProduct = value;
                this.OnPropertyChanged("IsInWebStore");
                this.OnPropertyChanged("ProductDataMismatch");
                this.OnPropertyChanged("UpdateTooltip");
                this.OnPropertyChanged("HasImage");
            }
        }
        private string _productTooltip = "TODO";
        public string ProductTooltip {
            get { return _productTooltip; }
            set {
                if (value == _productTooltip)
                    return;
                _productTooltip = value;
                this.OnPropertyChanged("ProductTooltip");
            }
        }

        public int productcode { get; set; }
        public string brandStr {
            get { return (_brand != null)? _brand.ToString() : ""; }
        }

        private IBrand _brand;
        public IBrand brand { 
            get { return _brand; }
            set {
                if (value == _brand)
                    return;
                _brand = value;
                this.OnPropertyChanged("brand");
            }
        }

        public string name { get; set; }
        public string fap { get; set; }
        public string family { get; set; }
        public string speciality { get; set; }
        public string tipo_venda { get; set; }
        public int iva_id { get; set; }
        public decimal pvp_lote { get; set; }
        public string gama { get; set; }
        public decimal pcusmed_prod { get; set; }
        public decimal pcus_prod { get; set; }
        public int stk_farmacia { get; set; }
        public int stk_min { get; set; }
        public int stk_max { get; set; }
        public bool esgotado { get; set; }
        public decimal pvpNoVat { get; set; }
        public int webstoreTaxId { get; set; }

        public string defaultNameEn { get { return name; } }
        public string defaultNamePt { get { return name; } }
        public string defaultDescriptionEn { get { return name; } }
        public string defaultDescriptionPt { get { return name; } }
        public string defaultMetaTitleEn { get { return URLUtils.GenerateSlug(0, $"{name}", false); } }
        public string defaultMetaTitlePt { get { return URLUtils.GenerateSlug(0, $"{name}", false); } }
        public string defaultMetaDescriptionEn { get { return $"{name}<br/>{fap}"; } }
        public string defaultMetaDescriptionPt { get { return $"{name}<br/>{fap}"; } }
        public string defaultMetaKeywordEn { get { return $"{productcode}, {name}"; } }
        public string defaultMetaKeywordPt { get { return $"{productcode}, {name}"; } }
        public string defaultMetaTagEn { get { return $"{productcode}, {name}"; } }
        public string defaultMetaTagPt { get { return $"{productcode}, {name}"; } }
        public string defaultSeoTagEn { get { return URLUtils.GenerateSlug(0, $"{ENGLISH_SHORT_CODE}-{name}", true); } }
        public string defaultSeoTagPt { get { return URLUtils.GenerateSlug(0, $"{name}", true); } }
        public ProductUpdateData productUpdateData { get; set; } = new ProductUpdateData();
        public void addCategoryToUpdateData(ICategory category) {
            productUpdateData.categories.Add(category);
            this.OnPropertyChanged("newCategoriesDescription");
            if (!IsSelected) 
                 IsSelected = IsSelectionEnabled = true;
        }
        public void removeCategoryFromUpdateData(ICategory category) {
            productUpdateData.categories.Remove(category);
            this.OnPropertyChanged("newCategoriesDescription");
            if(productUpdateData.categories.Count == 0 && !ProductDataMismatch)
                IsSelected = IsSelectionEnabled = false;
        }

        private bool _isSelectionEnabled = true;
        public bool IsSelectionEnabled {
            get {
                return this._isSelectionEnabled;
            }
            set {
                if (value == _isSelectionEnabled)
                    return;
                _isSelectionEnabled = value;
                this.OnPropertyChanged("IsSelectionEnabled");
            }
        }

        private bool _selected;
        public bool IsSelected {
            get {
                return this._selected;
            }
            set {
                if (value == _selected)
                    return;
                _selected = value;
                this.OnPropertyChanged("IsSelected");
                this.OnPropertyChanged("ProductDataMismatch");//Do this to reevaluate
                this.OnPropertyChanged("UpdateTooltip");
            }
        }

        public Image _imageFile;
        public Image ImageFile {
            get {
                return this._imageFile;
            }
            set {
                if (value == _imageFile)
                    return;
                _imageFile = value;
                this.OnPropertyChanged("ImageFile");
                this.OnPropertyChanged("HasNewImage");
            }
        }
        public bool HasNewImage {
            get {
                return _imageFile != null;
            }
        }
        public bool HasImage {
            get {
                return _webstoreProduct != null && _webstoreProduct.image != null && _webstoreProduct.image.Contains("http");
            }
        }
        public string existingCategoriesDescription {
            get {
                if(_webstoreProduct != null)
                    return _webstoreProduct.existingCategoriesDescription;
                else {
                    return String.Join(", ", categories.Select(cat => cat.name).ToArray());
                }
            }
        }
        public string newCategoriesDescription { 
            get {
                return String.Join(", ", productUpdateData.categories.Select(cat => $"+{cat.name}").ToArray());
            }
        }
        
        private List<ICategory> _categories = new List<ICategory>();
        public List<ICategory> categories { get {
                return _categories;
            } 
            set {
                _categories = value;       
            }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string prop) {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        //Color property
        private System.Windows.Media.Brush _itemColor = System.Windows.Media.Brushes.Transparent;
        public System.Windows.Media.Brush ItemBackColor {
            get { return _itemColor; }
            set {
                _itemColor = value;
                OnPropertyChanged("ItemBackColor");
            }
        }

        public string pvpStr
        {
            get
            {
                return $"€{pvp_lote}";
            }
        }
        public void build(IStore store, 
                int productcode,
                string name,
                string fap,
                string family,
                string speciality,
                string tipo_venda,
                int iva_id,
                decimal pvp_lote,
                string gama,
                decimal pcusmed_prod,
                decimal pcus_prod,
                int stk_farmacia,
                int stk_min,
                int stk_max,
                bool esgotado)
        {
            this.productcode = productcode;
            this.name = name;
            this.fap = fap;
            this.family = family;
            this.speciality = speciality;
            this.tipo_venda = tipo_venda;
            this.iva_id = iva_id;
            this.pvp_lote = pvp_lote;
            this.gama = gama;
            this.pcusmed_prod = pcusmed_prod;
            this.pcus_prod = pcus_prod;
            this.stk_farmacia = stk_farmacia;
            this.stk_min = stk_min;
            this.stk_max = stk_max;
            this.esgotado = esgotado;
            this.pvpNoVat = Math.Ceiling(Math.Round(100 * this.pvp_lote / (1 + this.webstoreVATTax(store)), 2)) / 100;
            this.webstoreTaxId = this.webstoreVATCode(store);
        }

        public int webstoreVATCode(IStore store) {
            if (store is Opencart) {
                switch (iva_id) {
                    case 5: return 3; //IVA23%
                    case 3: return 1; //IVA6%
                    case 6: return 1; //IVA6%
                    case 7: return 2; //IVA13%
                }
            }
            throw new Exception("webstoreVATCode not done for this type of store.");
        }
        public decimal webstoreVATTax(IStore store) {
            if (store is Opencart) {
                switch (iva_id) {
                    case 5: return (decimal)0.23; //IVA23%
                    case 3: return (decimal)0.06; //IVA6%
                    case 6: return (decimal)0.06; //IVA6%
                    case 7: return (decimal)0.13; //IVA13%
                }
            }
            throw new Exception("webstoreVATTax not done for this type of store.");
        }
    }

    public class ProductUpdateData {
        public string name { get; set; }
        public string description { get; set; }
        public string meta_desc { get; set; }
        public string meta_title { get; set; }
        public string meta_keyword { get; set; }
        public string meta_tag { get; set; }

        public string manufacturer_id { get; set; }

        public string title { get; set; }

        public List<ICategory> categories = new List<ICategory>();

        public override string ToString() {
            return $"name: {name} | title: {title} | meta_title: {meta_title} | description: {description} | meta_description: {meta_desc} | meta_keyword: {meta_keyword} | meta_tag: {meta_tag} | manufacturer_id: {manufacturer_id} | categories: {String.Join(",", categories.Select(o => $"[{o.category_id}] {o.name}"))}";
        }
    }

}
