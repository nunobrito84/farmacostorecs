﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.BillingSystem.Interfaces
{
    public abstract class BillingSystemBase
    {
        public delegate void logFunc(String str, bool isError = false);
        protected string connectionStr { get; set; }

        public BillingSystemBase(string connectionStr)
        {
            this.connectionStr = connectionStr;
        }
    }
}
