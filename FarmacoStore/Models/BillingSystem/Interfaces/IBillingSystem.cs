﻿using FarmacoStore.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FarmacoStore.Models.BillingSystem.Interfaces.BillingSystemBase;

namespace FarmacoStore.Models.BillingSystem.Interfaces
{
    interface IBillingSystem
    {
        string BsName { get; }
        IDbProduct ProductClass { get; }
        List<IDbProduct> getProducts(logFunc log);
    }
}
