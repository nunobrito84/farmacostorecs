﻿using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Interfaces
{
    public interface IDbProduct
    {
        string ENGLISH_SHORT_CODE { get; }// = "en";
        string PORTUGUESE_SHORT_CODE { get; }// = "pt";

        int productcode { get; set; }


        IBrand brand { get; set; }

        string name { get; set; }
        string fap { get; set; }
        string family { get; set; }
        string speciality { get; set; }
        string tipo_venda { get; set; }
        int iva_id { get; set; }
        decimal pvp_lote { get; set; }
        string gama { get; set; }
        decimal pcusmed_prod { get; set; }
        decimal pcus_prod { get; set; }
        int stk_farmacia { get; set; }
        int stk_min { get; set; }
        int stk_max { get; set; }
        bool esgotado { get; set; }
        decimal pvpNoVat { get; set; }
        int webstoreTaxId { get; set; }

        string existingCategoriesDescription { get; }
        List<ICategory> categories { get; set;  }

        ProductUpdateData productUpdateData { get; set; }
        void addCategoryToUpdateData(ICategory category);
        void removeCategoryFromUpdateData(ICategory category);

        bool IsInWebStore { get; }
        string UpdateTooltip { get; }

        int webstoreVATCode(IStore store);
        decimal webstoreVATTax(IStore store);

        bool ProductDataMismatch { get; }
        IProduct webstoreProduct { get; set; }
        event PropertyChangedEventHandler PropertyChanged;
        bool IsSelected { get; set; }

        bool IsSelectionEnabled { get; set; }
        string ProductTooltip { get; set; }

        bool HasNewImage { get; }
        bool HasImage { get; }
        Image ImageFile { get; set; }

        //Success or Error mark
        System.Windows.Media.Brush ItemBackColor { get; set; }

        string defaultNameEn { get; }
        string defaultNamePt { get; }
        string defaultDescriptionEn { get; }
        string defaultDescriptionPt { get; }
        string defaultMetaTitleEn { get; }
        string defaultMetaTitlePt { get; }
        string defaultMetaDescriptionEn { get; }
        string defaultMetaDescriptionPt { get; }
        string defaultMetaKeywordEn { get; }
        string defaultMetaKeywordPt { get; }
        string defaultMetaTagEn { get; }
        string defaultMetaTagPt { get; }
        string defaultSeoTagEn { get; }
        string defaultSeoTagPt { get; }
        void build(IStore store,
                int productcode,
                string name,
                string fap,
                string family,
                string speciality,
                string tipo_venda,
                int iva_id,
                decimal pvp_lote,
                string gama,
                decimal pcusmed_prod,
                decimal pcus_prod,
                int stk_farmacia,
                int stk_min,
                int stk_max,
                bool esgotado
            );
    }
}
