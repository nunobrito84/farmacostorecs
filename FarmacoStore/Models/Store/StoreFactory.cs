﻿using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store
{
    class StoreFactory
    {
        public static IStore Instance(string storeName, string baseUrl, string apiKey)
        {
            switch (storeName)
            {
                case "Opencart":
                    return new Opencart.Opencart(baseUrl, apiKey);
                case "Shopify":
                    return new Shopify.Shopify(baseUrl, apiKey);
                default:
                    throw new Exception($"{storeName} instance not implemented.");
            }
        }
        public static ICategory StoreCategory(string storeName, int id, int parent_id, string title, ObservableCollection<ICategory> children) {
            switch (storeName) {
                case "Opencart":
                    return new Opencart.Category(id, parent_id, title, children);
                case "Shopify":
                    return new Shopify.Category(id, parent_id, title, children);
                default:
                    throw new Exception($"{storeName} ICategory instance not implemented.");
            }
        }
        public static IBrand StoreBrand(string storeName, int id, string name, int sort_order, string image_url) {
            switch (storeName) {
                case "Opencart":
                    return new Opencart.Brand(id, name, sort_order, image_url);
                default:
                    throw new Exception($"{storeName} IBrand instance not implemented.");
            }
        }
        
        public static IProduct StoreProduct(string storeName, int id, string name) {
            switch (storeName) {
                //case "Shopify":
                //    return new Shopify.Produ(id, parent_id, title, children);
                case "Opencart":
                    return new Opencart.Product(id, name);
                default:
                    throw new Exception($"{storeName} IProduct instance not implemented.");
            }
        }
    }

}
