﻿using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FarmacoStore.Models.Store.Shopify
{
    public class Shopify : StoreBase, IStore {
        public Shopify(string baseUrl, string apiKey) : base(baseUrl, apiKey) {
        }
        public ObservableCollection<ICategory> getCategories(bool singleLanguage = true) {
            //https://shopify.dev/docs/admin-api/rest/reference/products/customcollection?api[version]=2020-04#index-2020-04
            //GET /admin/api/2020-04/custom_collections.json
            return new ObservableCollection<ICategory>(new ICategory[] {
                new Category(1, 0, "root", new ObservableCollection<ICategory>(new ICategory[]
                        { new Category(2, 1, "Dermocosmética", new ObservableCollection<ICategory>( new ICategory[]
                            { new Category(3, 2, "Corpo", null),
                              new Category(4, 2, "Pés", null),
                            })
                            ) ,
                         new Category(5, 1, "Solares", new ObservableCollection<ICategory>( new ICategory[]
                            { new Category(6, 5, "Bronzeadores", null),
                              new Category(7, 6, "Normais", null),
                            })
                            )
                        }
                    ))
                    }
                );
        }
        public ObservableCollection<IBrand> getBrands() {
            throw new Exception("getBrands not done for shopify");
        }
        public void insertCategory(ICategory category) {
            throw new Exception("insertCategory not done for shopify");
        }
        public ObservableCollection<IProduct> getProducts(FarmacoStore.Models.BillingSystem.Interfaces.BillingSystemBase.logFunc log) {
            throw new Exception("getProducts not done for shopify");
        }
        public void upsertProduct(IDbProduct product) {
            throw new Exception("upsertProduct not done for shopify");
        }
        public void insertProductImage(IDbProduct product) {
            throw new Exception("insertProductImage not done for shopify");
        }
        public void insertBrand(BrandSeed brandSeed) {
            throw new Exception("insertBrand not done for shopify");
        }
    }
}
