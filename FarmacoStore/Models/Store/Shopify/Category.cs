﻿using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Shopify
{
    public class Category : CategoryBase, ICategory {

        public Category(int id, int parent_id, string title, ObservableCollection<ICategory> children) : base(id, parent_id, title, children) {

        }
        public String toJson {
            get {
                throw new Exception("toJson not done for shopify");
            }
        }
    }
}
