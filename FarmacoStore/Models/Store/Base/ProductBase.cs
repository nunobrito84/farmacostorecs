﻿using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Base
{
    public abstract class ProductBase //: INotifyPropertyChanged
    {
        public int product_id { get; set; }
        public string name { get; set; }
        public int sku { get; set; }
        public string image { get; set; }
        public decimal priceNoVat { get; set; }
        public int tax_class_id { get; set; } //OC: 1=6% | 2=12% | 3=23%
        public int quantity { get; set; }

        public ProductBase(int id, string name)
        {
            this.product_id = id;
            this.name = name;
            //seo_keyword = URLUtils.GenerateSlug(id, name);
        }
        public void Initialize()
        {
        }
        public void index() {
            throw new Exception("Product index nor implemented.");
        }


    }
}
