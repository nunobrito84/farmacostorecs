﻿using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Base
{
    public abstract class BrandBase //: INotifyPropertyChanged
    {
        public int id_web_offset {
            get {
                //to 'obfuscate' the anf table id | Do not change this!!
                return 1234;
            }
        }
        public int brand_id { get; set; }
        public string name { get; set; }
        public string seo_keyword { get; set; }

        public BrandBase(int id, string name)
        {
            this.brand_id = id;
            this.name = name;
            seo_keyword = URLUtils.GenerateSlug(id, name);
        }
        public void Initialize() {
        }
        public override string ToString() {
            return name;
        }
    }
}
