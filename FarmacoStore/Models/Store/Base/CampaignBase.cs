﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Base {
    public class CampaignBase {
        public int campaign_id { get; set; }
        public string campaign_name { get; set; }
        public List<string> product_name_rules { get; set; }
        public List<int> product_code_rules { get; set; }
        public List<int> product_category_rules { get; set; }
        public decimal price { get; set; }
        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        public decimal discount_percent { get; set; }
        public decimal discount_absolute { get; set; }

    }
}
