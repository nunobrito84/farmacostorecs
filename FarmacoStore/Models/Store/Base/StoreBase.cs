﻿using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Base
{
    public abstract class StoreBase
    {
        protected string baseUrl { get; set; }
        protected string apiKey { get; set; }
        public StoreBase(string baseUrl, string apiKey)
        {
            this.baseUrl = baseUrl;
            this.apiKey = apiKey;
        }
        public int id { get; set; }
        public string title { get; set; }
        public ObservableCollection<ICategory> children { get; set; }
    }
}
