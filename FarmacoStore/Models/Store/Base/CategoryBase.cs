﻿using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Base
{
    public abstract class CategoryBase : INotifyPropertyChanged
    {
        public int category_id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string seo_keyword { get; set; }
        public int language_id { get; set; }

        private CategoryBase _parent;
        public ObservableCollection<ICategory> children { get; set; }

        public CategoryBase(int id, int parent_id, string name, ObservableCollection<ICategory> children)
        {
            this.category_id = id;
            this.parent_id = parent_id;
            this.name = name;
            this.children = children;
            seo_keyword = URLUtils.GenerateSlug(id, name);
        }
        public void Initialize()
        {
            if(children != null)
                foreach (CategoryBase child in this.children)
                {
                    child._parent = this;
                    child.Initialize();
                }
        }
        public void index() {
            throw new Exception("Category index nor implemented.");
        }

        private bool _isSelectionEnabled = true;
        public bool IsSelectionEnabled {
            get {
                return this._isSelectionEnabled;
            }
            set {
                this.SetSelectionEnabled(value);
            }
        }

        private bool _selected;
        public bool IsSelected {
            get
            {
                return this._selected;
            }
            set {
                this.SetIsChecked(value);
            }
        }
        public void SetIsChecked(bool value, bool updateChildren = true, bool updateParent = true)
        {
            //if (value == _selected)
            //    return;
            _selected = value;
            if (updateChildren && this.children != null)
                foreach (CategoryBase c in this.children) 
                    if(c.IsSelectionEnabled)
                        c.SetIsChecked(_selected, true, false);
            if (updateParent && _parent != null && _parent.IsSelectionEnabled)
                _parent.VerifyCheckState();
            this.OnPropertyChanged("IsSelected");
        }
        
        public void SetSelectionEnabled(bool value, bool updateChildren = false, bool updateParent = false) {
            //if (value == _isSelectionEnabled)
            //    return;
            _isSelectionEnabled = value;
            if (updateChildren && this.children != null)
                foreach (CategoryBase c in this.children)
                    c.SetSelectionEnabled(_isSelectionEnabled, true, false);
            if (updateParent && _parent != null)
                _parent.VerifyIsSelectionEnabledState();
            this.OnPropertyChanged("IsSelectionEnabled");
        }
        public static ICategory findCategory(ObservableCollection<ICategory> categories, int idToFind)
        {
            foreach (ICategory cat in categories)
            {
                if (cat.category_id == idToFind)
                    return cat;
                else
                {
                    ICategory foundCat = findCategory(cat.children, idToFind);
                    if (foundCat != null)
                        return foundCat;
                }
            }
            return null;
        }

        void VerifyCheckState()
        {
            for (int i = 0; i < this.children.Count; ++i)
            {
                if (this.children[i].IsSelected)
                {
                    this.SetIsChecked(true, false, true);
                    break;
                }
            }
            if (_parent != null && _parent.IsSelectionEnabled) //recursive
                _parent.VerifyCheckState();
        }
        void VerifyIsSelectionEnabledState() {
            for (int i = 0; i < this.children.Count; ++i) {
                if (this.children[i].IsSelectionEnabled) {
                    this.SetSelectionEnabled(true, false, true);
                    break;
                }
            }
            if (_parent != null) //recursive
                _parent.VerifyIsSelectionEnabledState();
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string prop)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        //Color property
        private System.Windows.Media.Brush _itemColor = System.Windows.Media.Brushes.Transparent;
        public System.Windows.Media.Brush ItemBackColor {
            get { return _itemColor; }
            set {
                _itemColor = value;
                OnPropertyChanged("ItemBackColor");
            }
        }
    }
}
