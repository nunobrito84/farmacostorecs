﻿using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Utils;

/// https://api.opencart-api.com/demo/admin/#/
namespace FarmacoStore.Models.Store.Opencart {
    public class Opencart : StoreBase, IStore {
        private const String CATEGORIES_INDEX = "/index.php?route=fcoapi/rest/category_admin/index";
        private const String CATEGORY_INSERT = "/index.php?route=fcoapi/rest/category_admin/category";

        private const String BRANDS_INDEX = "/index.php?route=fcoapi/rest/manufacturer_admin/manufacturer&limit=1000000&page=1";
        private const String BRAND_INSERT = "/index.php?route=fcoapi/rest/manufacturer_admin/manufacturer";
        private const String BRAND_INSERT_LOGO = "/index.php?route=fcoapi/rest/manufacturer_admin/manufacturerimages";

        private const String PRODUCTS_INDEX = "/index.php?route=fcoapi/rest/product_admin/products";
        private const String PRODUCT_UPDATE = "/index.php?route=fcoapi/rest/product_admin/products";
        private const String PRODUCT_INSERT_IMAGE = "/index.php?route=fcoapi/rest/product_admin/productimages";
                    
        public const int ENGLISH_ID = 1;
        public const int PORTUGUESE_ID = 2;

        //Attribute
        public enum ATTRIBUTES : int {
            FAP = 1,
            FAM,
            SPEC,
            RANGE,
            COD_CLASS1,
            COD_CLASS2,
            COD_CLASS3,
            CLASS1,
            CLASS2,
            CLASS3,
            BRAND_CODE,
            BRAND
        }

        public Opencart(string baseUrl, string apiKey) : base(baseUrl, apiKey) {
        }
        public ObservableCollection<ICategory> getCategories(bool singleLanguage = true) {
            //GET https://store.hopto.org/index.php?route=fcoapi/rest/category_admin/index
            /*
            {
                "categories": [
                    {
                        "category_id": "25",
                        "thumb": "",
                        "parent_id": "0",
                        "top": "1",
                        "column": "1",
                        "sort_order": "3",
                        "status": "1",
                        "date_added": "2009-01-31 01:04:25",
                        "date_modified": "2011-05-30 12:14:55",
                        "language_id": "1",
                        "name": "Components",
                        "description": "",
                        "meta_title": "Components",
                        "meta_description": "",
                        "meta_keyword": ""
                    }
                ]
            }*/

            string requestUrl = baseUrl + CATEGORIES_INDEX;
            Dictionary<string, dynamic> responseData;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
            request.Method = "GET";
            request.Headers["X-Oc-Restadmin-Id"] = apiKey;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            var categories_json = responseData["categories"];

            List<Category> categories = new List<Category>();
            //Convert to array of categories and sort
            foreach (Dictionary<string, dynamic> cat in categories_json) {
                if(singleLanguage == false || Int32.Parse(cat["language_id"]) == PORTUGUESE_ID)
                    categories.Add(new Category(cat));
            }
            categories = categories.OrderBy(o => o.parent_id).ToList();

            return new ObservableCollection<ICategory>(categories);
           
        }
        public ObservableCollection<IBrand> getBrands() {
            //GET https://store.hopto.org/index.php?route=fcoapi/rest/manufacturer_admin/manufacturer&limit=1000000&page=1
            string requestUrl = baseUrl + BRANDS_INDEX;
            Dictionary<string, dynamic> responseData;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
            request.Method = "GET";
            request.Headers["X-Oc-Restadmin-Id"] = apiKey;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            if(responseData["success"] == false) {
                if (responseData["error"] == "No manufacturers found")
                    return new ObservableCollection<IBrand>();
                else throw new Exception(responseData["error"]);
            }
            var brands_json = responseData["data"];
            List<Brand> brands = new List<Brand>();
            //Convert to array of categories and sort
            foreach (Dictionary<string, dynamic> brand in brands_json)
               brands.Add(new Brand(brand));
            brands = brands.OrderBy(o => o.brand_id).ToList();
            
            return new ObservableCollection<IBrand>(brands);

        }
        public void insertCategory(ICategory category) {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(baseUrl + CATEGORY_INSERT);
            request.Method = "POST";
            request.Headers["X-Oc-Restadmin-Id"] = apiKey;
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                streamWriter.Write(category.toJson);
            }
            Dictionary<string, dynamic> responseData;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            if (responseData["success"] == true) {
                
            } else {
                throw new Exception($"Error inserting category #{category.category_id}. Cause: {responseData["error"]}");
            }
            Console.WriteLine(responseData);
        }

        public ObservableCollection<IProduct> getProducts(FarmacoStore.Models.BillingSystem.Interfaces.BillingSystemBase.logFunc log) {
            int page = 1;
            const int LIMIT = 100;
            List<Product> products = new List<Product>();

            for(;;page++) {
                string requestUrl = baseUrl + PRODUCTS_INDEX + $"&limit=${LIMIT}&page={page}";
                Dictionary<string, dynamic> responseData;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["X-Oc-Restadmin-Id"] = apiKey;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var s = new JavaScriptSerializer();
                    responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                    reader.Close();
                    dataStream.Close();
                }
                if (responseData["success"] == false && responseData["error"] == "No product found")//Empty list
                    break;//Finished loading

                var products_json = responseData["data"];
                //Convert to array of categories and sort
                int count = 0;
                foreach (Dictionary<string, dynamic> product in products_json) {
                    int unusedSku;
                    if (int.TryParse(product["sku"], out unusedSku)) {
                        products.Add(new Product(product));
                    }
                    count++;
                }
                products = products.OrderBy(o => o.sku).ToList();
                Console.WriteLine($"Loaded {products.Count} products from webstore");
                log($"Loaded {products.Count} products from webstore");
                if (count < LIMIT)
                    break;
            }

            return new ObservableCollection<IProduct>(products);
        }

        public void upsertProduct(IDbProduct product) {
            Dictionary<string, dynamic> responseData;
            if (product.IsInWebStore) { //Edit - Put
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(baseUrl + PRODUCT_UPDATE + $"&id={product.webstoreProduct.product_id}");
                request.Method = "PUT";
                request.Headers["X-Oc-Restadmin-Id"] = apiKey;
                request.ContentType = "application/json";

                using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    dynamic obj = new System.Dynamic.ExpandoObject();
                    obj.quantity = product.stk_farmacia;
                    obj.price = product.pvpNoVat;
                    obj.tax_class_id = product.webstoreVATCode(this);
                    //only switch between 7 (in stock) and 5 (no stock) to allow letting pre-orders occur on manually selected items
                    //6: 2-3 dias
                    obj.stock_status_id = (product.stk_farmacia > 0 ? 7 : (product.webstoreProduct.allowsPreorder? 6 : 5 )); 
                    if (product.productUpdateData != null) {
                        obj.product_description = new[] {
                            new {
                                language_id = ENGLISH_ID,
                                name = product.productUpdateData.name ?? product.webstoreProduct.name,
                                description = product.productUpdateData.description ?? product.webstoreProduct.description,
                                meta_description = product.productUpdateData.meta_desc ?? product.webstoreProduct.meta_description,
                                meta_title = product.productUpdateData.meta_title ?? product.webstoreProduct.meta_title,
                                meta_keyword = product.productUpdateData.meta_keyword ?? product.webstoreProduct.meta_keyword,
                                tag = product.productUpdateData.meta_tag ?? product.webstoreProduct.tags,
                            },
                            new {
                                language_id = PORTUGUESE_ID,
                                name = product.productUpdateData.name ?? product.webstoreProduct.name,
                                description = product.productUpdateData.description ?? product.webstoreProduct.description,
                                meta_description = product.productUpdateData.meta_desc ?? product.webstoreProduct.meta_description,
                                meta_title = product.productUpdateData.meta_title ?? product.webstoreProduct.meta_title,
                                meta_keyword = product.productUpdateData.meta_keyword ?? product.webstoreProduct.meta_keyword,
                                tag = product.productUpdateData.meta_tag ?? product.webstoreProduct.tags,
                            },
                        };
                        if (product.productUpdateData.name != null)
                            obj.model = product.productUpdateData.name;
                        if (product.productUpdateData.manufacturer_id != null)
                            obj.manufacturer_id = Int32.Parse(product.productUpdateData.manufacturer_id);

                        if (product.productUpdateData.categories.Count > 0)
                            obj.product_category = product.categories.Select(o => o.category_id).ToArray().Concat(product.productUpdateData.categories.Select(o => o.category_id).ToArray());
                    }
                    string productJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    streamWriter.Write(productJson);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var s = new JavaScriptSerializer();
                    responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                    reader.Close();
                    dataStream.Close();
                }
                if (responseData["success"] == true) {
                    Console.WriteLine("Product update success");
                } else {
                    throw new Exception($"Error updating product #{product.webstoreProduct.product_id}. Cause: {responseData["error"]["keyword"]}");
                }
                Console.WriteLine(responseData);
            } else {//Insert - Post
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(baseUrl + PRODUCT_UPDATE);
                request.Method = "POST";
                request.Headers["X-Oc-Restadmin-Id"] = apiKey;
                request.ContentType = "application/json";

                using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    string productJson = JsonConvert.SerializeObject(
                        new {
                            model = StringUtil.Truncate(product.defaultNamePt,64),//Limited to 64 chars on OC3
                            quantity = product.stk_farmacia,
                            price = product.pvpNoVat,// Send price without vat
                            tax_class_id = product.webstoreVATCode(this),//1=IVA6% | 2=IVA12% | 3=IVA23%
                            manufacturer_id = product.brand?.web_id, //use obfuscated id
                            sku = product.productcode,
                            status = 1,
                            points = 0,
                            reward = 0,
                            image = "",
                            //other_images = [
                            //    ""
                            //],
                            shipping = 1,
                            stock_status_id = product.stk_farmacia > 0 ? 7 : 5, // 7 = In Stock | 8 = Pre-order | 5 = Out of stock | 6 = 2-3 days
                            upc = "",
                            ean = "",
                            jan = "",
                            isbn = "",
                            mpn = "",
                            location = "",
                            date_available = DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                            weight = 0,
                            weight_class_id = 1, //1 = Kg
                            length = 0,
                            width = 0,
                            height = 0,
                            length_class_id = 1,  //1 = Centimeter
                            subtract = 1,//1 = Reduce the stock internally in the webstore
                            minimum = 1,
                            sort_order = 1,
                            product_store = new[] { 0 },
                            //product_related = new[] {
                            //    34
                            //},
                            //product_filter = new[] {
                            //    34
                            //},
                            seo_keywords = new[] {
                                new {
                                    keyword = product.defaultSeoTagEn,
                                    language_id= 1,//en-gb
                                },
                                new {
                                    keyword = product.defaultSeoTagPt,
                                    language_id= 2,//pt-br
                                }
                            },
                            product_description = new[] {
                                    new {
                                        language_id = ENGLISH_ID,
                                        name = product.defaultNameEn,
                                        description = product.defaultDescriptionEn,
                                        meta_title = product.defaultMetaTitleEn,
                                        meta_description = product.defaultMetaDescriptionEn,
                                        meta_keyword = product.defaultMetaKeywordEn,
                                        tag = product.defaultMetaTagEn
                                    },
                                    new {
                                        language_id = PORTUGUESE_ID,
                                        name = product.defaultNamePt,
                                        description = product.defaultDescriptionEn,
                                        meta_title = product.defaultMetaTitlePt,
                                        meta_description = product.defaultMetaDescriptionPt,
                                        meta_keyword = product.defaultMetaKeywordEn,
                                        tag = product.defaultMetaTagPt
                                    }
                                },
                            product_category = product.categories.Select(o => o.category_id).ToArray().Concat(product.productUpdateData.categories.Select(o => o.category_id).ToArray()),
                            product_attribute = new[] {
                              new {
                                attribute_id = ATTRIBUTES.FAP,
                                product_attribute_description = new[] {
                                    new {
                                        language_id = PORTUGUESE_ID,
                                        text = product.fap
                                    },
                                    new {
                                        language_id = ENGLISH_ID,
                                        text = product.fap
                                    }
                                }
                              },
                              new {
                                attribute_id = ATTRIBUTES.FAM,
                                product_attribute_description = new[] {
                                    new {
                                        language_id = PORTUGUESE_ID,
                                        text = product.family
                                    },
                                    new {
                                        language_id = ENGLISH_ID,
                                        text = product.family
                                    }
                                }
                              },
                              new {
                                attribute_id = ATTRIBUTES.SPEC,
                                product_attribute_description = new[] {
                                    new {
                                        language_id = PORTUGUESE_ID,
                                        text = product.speciality
                                    },
                                    new {
                                        language_id = ENGLISH_ID,
                                        text = product.speciality
                                    }
                                }
                              },
                            }
                        }
                        , Formatting.Indented);
                    streamWriter.Write(productJson);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var s = new JavaScriptSerializer();
                    responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                    reader.Close();
                    dataStream.Close();
                }
                if (responseData["success"] == true) {
                    Console.WriteLine("Product update success");
                } else {
                    throw new Exception($"Error updating product #{product.webstoreProduct.product_id}. Cause: {responseData["error"]["keyword"]}");
                }
                Console.WriteLine(responseData);
            }
        }

        public void insertProductImage(IDbProduct product) {
            MemoryStream imageStream = new MemoryStream();
            product.ImageFile.Save(imageStream, ImageFormat.Jpeg);
            imageStream.Position = 0;
            // boundary character
            string fileName = URLUtils.GenerateSlug(product.productcode, product.name);
            var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(baseUrl + PRODUCT_INSERT_IMAGE + $"&id={product.webstoreProduct.product_id}");
            webRequest.Method = "POST";
            webRequest.Headers["X-Oc-Restadmin-Id"] = apiKey;
            webRequest.Timeout = 60000;
            webRequest.ContentType = "multipart/form-data; boundary=" + boundary;
            // start boundary character
            var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary + "\r\n");
            // End character
            var endBoundary = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n");
            var newLineBytes = Encoding.UTF8.GetBytes("\r\n");
            using (var stream = new MemoryStream()) {
                // Write start boundary character
                stream.Write(beginBoundary, 0, beginBoundary.Length);
                // write to file
                var fileHeader = $"Content-Disposition: form-data; name=\"file\"; filename=\"{fileName}.jpg\"\r\n" +
                                 "Content-Type: image/jpeg\r\n\r\n";
                var fileHeaderBytes = Encoding.UTF8.GetBytes(string.Format(fileHeader, fileName));
                stream.Write(fileHeaderBytes, 0, fileHeaderBytes.Length);
                stream.Write(imageStream.GetBuffer(), 0, imageStream.GetBuffer().Length);
                stream.Write(newLineBytes, 0, newLineBytes.Length);
                stream.Write(endBoundary, 0, endBoundary.Length);
                webRequest.ContentLength = stream.Length;
                stream.Position = 0;
                var tempBuffer = new byte[stream.Length];
                stream.Read(tempBuffer, 0, tempBuffer.Length);
                using (Stream requestStream = webRequest.GetRequestStream()) {
                    requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                    using (var response = webRequest.GetResponse())
                    using (StreamReader httpStreamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8)) {
                        var s = new JavaScriptSerializer();
                        Dictionary<string, dynamic> responseData = s.Deserialize<Dictionary<string, dynamic>>(httpStreamReader.ReadToEnd());
                        if (responseData["success"] == true) {
                            Console.WriteLine("Product image insert success");
                        } else {
                            throw new Exception($"Error inserting product image #{product.webstoreProduct.product_id}. Cause: {responseData["error"]["keyword"]}");
                        }
                    }
                }

            }
        }
        public void insertBrand(BrandSeed brandSeed) {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(baseUrl + BRAND_INSERT);
            request.Method = "POST";
            request.Headers["X-Oc-Restadmin-Id"] = apiKey;
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                streamWriter.Write(new Brand(brandSeed.brand_id, brandSeed.name, 0, "").toJson);
            }
            Dictionary<string, dynamic> responseData;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            if (responseData["success"] == true) {
                insertBrandImage(brandSeed); //Send Logo
            } else {
                throw new Exception($"Error inserting brand #{brandSeed.brand_id}. Cause: {responseData["error"]}");
            }
            Console.WriteLine(responseData);
        }
        private void insertBrandImage(BrandSeed brandSeed) {
            MemoryStream imageStream = new MemoryStream();
            brandSeed.image.Save(imageStream, ImageFormat.Png);
            imageStream.Position = 0;
            // boundary character
            string fileName = URLUtils.GenerateSlug(brandSeed.brand_id, brandSeed.name);
            var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(baseUrl + BRAND_INSERT_LOGO + $"&id={brandSeed.web_id}");
            webRequest.Method = "POST";
            webRequest.Headers["X-Oc-Restadmin-Id"] = apiKey;
            webRequest.Timeout = 60000;
            webRequest.ContentType = "multipart/form-data; boundary=" + boundary;
            // start boundary character
            var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary + "\r\n");
            // End character
            var endBoundary = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n");
            var newLineBytes = Encoding.UTF8.GetBytes("\r\n");
            using (var stream = new MemoryStream()) {
                // Write start boundary character
                stream.Write(beginBoundary, 0, beginBoundary.Length);
                // write to file
                var fileHeader = $"Content-Disposition: form-data; name=\"file\"; filename=\"{fileName}.png\"\r\n" +
                                 "Content-Type: image/png\r\n\r\n";
                var fileHeaderBytes = Encoding.UTF8.GetBytes(string.Format(fileHeader, fileName));
                stream.Write(fileHeaderBytes, 0, fileHeaderBytes.Length);
                stream.Write(imageStream.GetBuffer(), 0, imageStream.GetBuffer().Length);
                stream.Write(newLineBytes, 0, newLineBytes.Length);
                stream.Write(endBoundary, 0, endBoundary.Length);
                webRequest.ContentLength = stream.Length;
                stream.Position = 0;
                var tempBuffer = new byte[stream.Length];
                stream.Read(tempBuffer, 0, tempBuffer.Length);
                using (Stream requestStream = webRequest.GetRequestStream()) {
                    requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                    using (var response = webRequest.GetResponse())
                    using (StreamReader httpStreamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8)) {
                        var s = new JavaScriptSerializer();
                        Dictionary<string, dynamic> responseData = s.Deserialize<Dictionary<string, dynamic>>(httpStreamReader.ReadToEnd());
                        if (responseData["success"] == true) {
                            Console.WriteLine("Product image insert success");
                        } else {
                            throw new Exception($"Error inserting brand image #{brandSeed.brand_id}. Cause: {responseData["error"]["keyword"]}");
                        }
                    }
                }

            }
        }

    }
}
