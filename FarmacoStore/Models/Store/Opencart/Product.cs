﻿using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Opencart
{
    public class Product : ProductBase, IProduct
    {
        public string manufacturer;
        public string[] alt_images;

        public decimal rating; //Customers evaluation rating
        public string barcode; //upc   
        public int _manufacturer_id;//Use the sifarma codes                 
        
        public int status = 1; //1=enabled 0=disabled
        public DateTime date_available = new DateTime();

        public int currency_id = 3;//3 = EUR
        public string currency_code = "EUR";
        public string currency_value = "1.00000000";
        
        public string keyword;// seo keyword

        public string description {
            get {
                return productDescriptions.FirstOrDefault(o => o.language_id == Opencart.PORTUGUESE_ID)?.description;
            }
        }
        public string meta_description {
            get {
                return productDescriptions.FirstOrDefault(o => o.language_id == Opencart.PORTUGUESE_ID)?.meta_description;
            }
        }
        public string meta_title {
            get {
                return productDescriptions.FirstOrDefault(o => o.language_id == Opencart.PORTUGUESE_ID)?.meta_title;
            }
        }
        public string meta_keyword {
            get {
                return productDescriptions.FirstOrDefault(o => o.language_id == Opencart.PORTUGUESE_ID)?.meta_keyword;
            }
        }
        public string tags {
            get {
                return productDescriptions.FirstOrDefault(o => o.language_id == Opencart.PORTUGUESE_ID)?.tag;
            }
        }
        public int manufacturer_id {
            get {
                return _manufacturer_id;
            }
        }

        public string existingCategoriesDescription {
            get {
                return String.Join(", ", productCategories.Select(p => p.name).ToArray());
            }
        }

        public List<ProductDescription> productDescriptions = new List<ProductDescription>();
        public List<ProductAttribute> productAttributes = new List<ProductAttribute>();
        public List<ProductCategory> productCategories = new List<ProductCategory>();

        public class ProductCategory {
            public int category_id;
            public string name;
            public string description;
            public string sort_order;
            public string meta_title;
            public string meta_description;
            public string meta_keyword;
            public int language_id;
            public ProductCategory(Dictionary<string, dynamic> json) {
                category_id = Int32.Parse(json["category_id"]);
                name = json["name"];
                description = json["description"];
                sort_order = json["sort_order"];
                meta_title = json["meta_title"];
                meta_description = json["meta_description"];
                meta_keyword = json["meta_keyword"];
                language_id = Int32.Parse(json["language_id"]);
            }
        }
        public class ProductDescription {
            public int language_id;
            public string name;
            public string description;
            public string meta_description;
            public string meta_keyword;
            public string meta_title;
            public string tag;
            public ProductDescription(Dictionary<string, dynamic> json) {
                language_id = Int32.Parse(json["language_id"]);
                name = json["name"];
                description = json["description"];
                meta_description = json["meta_description"];
                meta_keyword = json["meta_keyword"];
                meta_title = json["meta_title"];
                tag = json["tag"];
            }
        }

        /*
        public List<CampaignBase> productSpecialPrice = new List<CampaignBase>();

        public List<CampaignBase> campaigns {
            get {
                return productSpecialPrice;
            }
            set {
                productSpecialPrice = value;
            }
        }
        public class ProductSpecialPrice : CampaignBase {
            public int customer_group_id;
            public int priority;

            public ProductSpecialPrice(Dictionary<string, dynamic> json) {
                customer_group_id = Int32.Parse(json["customer_group_id"]);
                priority = Int32.Parse(json["priority"]);
                price = Math.Round((decimal)json["price"], 2);
                date_start = DateTime.ParseExact(json["date_start"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                date_end = DateTime.ParseExact(json["date_end"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                campaign_id = json["special_id"];
                campaign_name = json["special_name"];
                discount_percent = json["discount_percent"];
                discount_absolute = json["discount_absolute"];
                int tmp;
                string names = ((string)json["product_name_rules"]);
                product_name_rules = (names == null || names.Length == 0) ? new List<string>() : names.Split(',').ToList();
                string codes = ((string)json["product_code_rules"]);
                product_code_rules = (codes == null || codes.Length == 0) ? new List<int>() : codes.Split(',').Select(str => int.TryParse(str, out tmp) ? tmp : 0).ToList();
                string categories = ((string)json["product_category_rules"]);
                product_category_rules = (categories == null || categories.Length == 0) ? new List<int>() : categories.Split(',').Select(str => int.TryParse(str, out tmp) ? tmp : 0).ToList();
            }
        }
        */
        public class ProductAttribute {
            public int attribute_id;
            public List<ProductAttributeDetails> details = new List<ProductAttributeDetails>();
            public ProductAttribute(KeyValuePair<string, dynamic> json) {
                attribute_id = Int32.Parse(json.Key);
                foreach (var attribute_detail in json.Value) 
                    details.Add(new ProductAttribute.ProductAttributeDetails(attribute_detail.Value));
            }

            public class ProductAttributeDetails {
                public string name;
                public string text;
                public int attribute_group_id;
                public int language_id;
                public ProductAttributeDetails(Dictionary<string, dynamic> json) {
                    name = json["name"];
                    text = json["text"];
                    attribute_group_id = Int32.Parse(json["attribute_group_id"]);
                    language_id = Int32.Parse(json["language_id"]);
                }
            }
        }

        public Product(int id, string name) : base(id, name)
        {
        }

        public bool allowsPreorder {
            get {
                foreach (ProductAttribute attribute in this.productAttributes) {
                    foreach (ProductAttribute.ProductAttributeDetails attributeDetail in attribute.details) {
                        if (attributeDetail.name == "Preorder")
                            return true;
                    }
                }
                return false;
            }
        }

        public Product(Dictionary<string, dynamic> json) : base(0, "") {

            CultureInfo provider = CultureInfo.InvariantCulture;
            base.product_id = Int32.Parse(json["id"]);
            base.name = json["model"];
            base.sku = Int32.Parse(json["sku"]);

            manufacturer = json["manufacturer"];
            image = json["image"];
            //alt_images = json[""];
            priceNoVat = Math.Ceiling(Math.Round((decimal)(100*(json["price"] - json["tax_value"])), 2)) / 100;
            barcode = json["upc"]; //upc   
            _manufacturer_id = (json["manufacturer_id"] != null)? Int32.Parse(json["manufacturer_id"]) : 0;//Use the sifarma codes                 
            tax_class_id = Int32.Parse(json["tax_class_id"]);//1=6% | 2=12% | 3=23%
            status = Int32.Parse(json["status"]); //1=enabled 0=disabled
            date_available = DateTime.ParseExact(json["date_available"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
            quantity = Int32.Parse(json["quantity"].ToString());

            currency_id = Int32.Parse(json["currency_id"]);//3 = EUR
            currency_code = json["currency_code"];
            currency_value = json["currency_value"];
            keyword = json["keyword"];// seo keyword
            
            foreach(var product_description_array in json["product_description"]) 
                productDescriptions.Add(new ProductDescription(product_description_array.Value));

            //foreach (var product_specials_array in json["special"])
            //    productSpecialPrice.Add(new ProductSpecialPrice(product_specials_array));

            foreach (var product_category_array in json["category"])
                foreach (var product_category in product_category_array.Value) {
                    if(product_category["language_id"] == "2")//Add if PT
                        productCategories.Add(new ProductCategory(product_category));
                }
            if (json["product_attributes"].Count > 0) {
                foreach (var attributes_array in json["product_attributes"]["attributes"])
                    productAttributes.Add(new ProductAttribute(attributes_array));
            }
            
        }
 
    }
}
