﻿using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Opencart
{
    public class Category : CategoryBase, ICategory
    {
        public string thumb { get; set; }
        public int top { get; set; }
        public int column { get; set; }
        public int sort_order { get; set; }
        public int status { get; set; }
        public DateTime date_added { get; set; }
        public DateTime date_modified { get; set; }
        public string description { get; set; }
        public string meta_title { get; set; }
        public string meta_description { get; set; }
        public string meta_keyword { get; set; }

        public Category(int id, int parent_id, string title, ObservableCollection<ICategory> children) : base(id, parent_id, title, children)
        {
            top = parent_id == 0 ? 1 : 0;
            meta_description = title;
            meta_title = title;
            meta_keyword = URLUtils.GenerateSlug(id, title);
            language_id = 2;//Must use this default for PT
            status = 1;//Enabled to show on Frontoffice
        }

        public Category(Dictionary<string, dynamic> json) : base(0, 0, "", new ObservableCollection<ICategory>()) {
            CultureInfo provider = CultureInfo.InvariantCulture;
            base.category_id = Int32.Parse(json["category_id"]);
            base.parent_id = Int32.Parse(json["parent_id"]);//
            base.name = json["name"];
             
            thumb = json["thumb"];
            top = Int32.Parse(json["top"]);//": "0",
            column = Int32.Parse(json["column"]);//": "0",
            sort_order = Int32.Parse(json["sort_order"]);//": "2",
            status = Int32.Parse(json["status"]);//": "1",
            date_added = DateTime.ParseExact(json["date_added"], "yyyy-MM-dd HH:mm:ss", provider);//": "2009-01-31 01:55:34",
            date_modified = DateTime.ParseExact(json["date_modified"], "yyyy-MM-dd HH:mm:ss", provider);//": "2010-08-22 06:32:15",
            language_id = Int32.Parse(json["language_id"]);//": "1",
            description = json["description"];//": "",
            meta_title = json["meta_title"];//": "Mac",
            meta_description = json["meta_description"];//": "",
            meta_keyword = json["meta_keyword"];//": ""

            //Dont allow selection because it comes from the store
            SetSelectionEnabled(false);
        }

        public String toJson {
            get {

                /*
                {
                    "category_description": [
                        {
                            "name": "Demo category",
                            "description": "Description of the category",
                            "language_id": 1,
                            "meta_description": "Meta description of the category",
                            "meta_keyword": "demo, demo2",
                            "meta_title": "meta-title"
                        }
                    ],
                    "image": "",
                    "sort_order": 0,
                    "category_store": [
                        0
                    ],
                    "parent_id": 0,
                    "status": 1,
                    "category_filter": [
                        0
                    ],
                    "column": 1,
                    "top": 1,
                    "keyword": "seo-category-url-2"
                }
                */

                var category = new {
                    category_id = category_id,
                    category_description = new[] {
                        new {
                            name = name,
                            description = description,
                            language_id= Opencart.ENGLISH_ID,//en-gb
                            meta_description = meta_description,
                            meta_keyword = meta_keyword,
                            meta_title = meta_title
                        },
                        new {
                                name = name,
                                description = description,
                                language_id= Opencart.PORTUGUESE_ID,//pt-PT
                                meta_description = meta_description,
                                meta_keyword = meta_keyword,
                                meta_title = meta_title
                        }
                    },
                    seo_keywords = new[] {
                        new {
                            keyword = seo_keyword + "-en",
                            language_id= 1,//en-gb
                        },
                        new {
                            keyword = seo_keyword,
                            language_id= 2,//pt-br
                        }
                    },
                    //image = thum,
                    sort_order = sort_order,
                    //category_store = new[] {
                    //    0
                    //},
                    parent_id = parent_id,
                    status = status,
                    //category_filter = new[] {
                    //    0
                    //},
                    language_id = language_id,
                    column = column,
                    top = top
                };

                string json = JsonConvert.SerializeObject(category, Formatting.Indented);
                return json;
            }
        }

    }
}
