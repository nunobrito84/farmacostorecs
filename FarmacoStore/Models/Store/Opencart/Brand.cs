﻿using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Opencart
{
    public class Brand : BrandBase, IBrand
    {
        public int web_id {
            get {
                return brand_id - id_web_offset; 
            }
        }
        public int sort_order { get; set; }
        public string image_url { get; set; }

        public Brand(int id, string name, int sort_order, string image_url) : base(id, name)
        {
            this.sort_order = sort_order;
            this.image_url = image_url;
        }

        public Brand(Dictionary<string, dynamic> json) : base(0,"") {
            CultureInfo provider = CultureInfo.InvariantCulture;
            base.brand_id = Int32.Parse(json["manufacturer_id"]) + id_web_offset; // deobfuscate
            base.name = json["name"];
            sort_order = Int32.Parse(json["sort_order"]);
            image_url = json["image"];
        }
        public override string ToString() {
            return base.name;
        }
        public String toJson {
            get {
                var manufacturer = new {
                    manufacturer_id = web_id, //use web_id
                    name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name),
                    sort_order = sort_order,
                    manufacturer_store = new[] {
                        0
                    },
                    seo_keywords = new[] {
                        new {
                            keyword = seo_keyword + "-en",
                            language_id= 1,//en-gb
                        },
                        new {
                            keyword = seo_keyword,
                            language_id= 2,//pt-br
                        }
                    }
                };
                string json = JsonConvert.SerializeObject(manufacturer, Formatting.Indented);
                return json;
            }
        }

    }
}
