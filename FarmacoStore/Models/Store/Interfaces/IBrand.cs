﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Interfaces
{
    public interface IBrand
    {
        int web_id { get; } //obfuscated web id
        int brand_id { get; set; } 
        string name { get; set; }

        void Initialize();

    }
}
