﻿using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store
{
    public interface IStore
    {
        ObservableCollection<ICategory> getCategories(bool singleLanguage = true);
        ObservableCollection<IBrand> getBrands();
        
        void insertCategory(ICategory category);

        ObservableCollection<IProduct> getProducts(FarmacoStore.Models.BillingSystem.Interfaces.BillingSystemBase.logFunc log);

        void upsertProduct(IDbProduct product);
        void insertProductImage(IDbProduct product);
        void insertBrand(BrandSeed brandSeed);
    }
}
