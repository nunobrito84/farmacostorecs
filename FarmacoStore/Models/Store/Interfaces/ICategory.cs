﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Interfaces
{
    public interface ICategory
    {
        event PropertyChangedEventHandler PropertyChanged;
        int category_id { get; set; } 
        int parent_id { get; set; }
        string name { get; set; }
        string seo_keyword { get; set; }
        bool IsSelected { get; set; }
        void SetIsChecked(bool value, bool updateChildren, bool updateParent);

        int language_id { get; set; }

        bool IsSelectionEnabled { get; set;  }
        void SetSelectionEnabled(bool value, bool updateChildren = false, bool updateParent = false);

        //Success or Error mark
        System.Windows.Media.Brush ItemBackColor { get; set; }

        ObservableCollection<ICategory> children { get; set; }
        void Initialize();

        String toJson { get; }
    }
}
