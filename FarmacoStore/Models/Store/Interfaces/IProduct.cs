﻿using FarmacoStore.Models.Store.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Models.Store.Interfaces
{
    public interface IProduct
    {
        int product_id { get; set; } 
        string name { get; set; }
        string description { get; }

        string meta_description { get; }
        string meta_title { get; }
        string meta_keyword { get; }
        string tags { get; }

        bool allowsPreorder { get; }

        int manufacturer_id { get; }

        int sku { get; set; }
        string image { get; set; }

        decimal priceNoVat { get; set; }
        string existingCategoriesDescription { get; }

        int tax_class_id { get; set; } //1=6% | 2=12% | 3=23%
        int quantity { get; set; }

        //List<CampaignBase> campaigns { get; set; }

        void Initialize();

    }
}
