﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Utils;

namespace FarmacoStore.Models {
    public class ServerCredentials {
        const String NodeName = "FarmacoStore";
        private string _pharma_name;
        private string _base_url;
        private string _api_key;
        private string _api_username;
        private string _db_conn;
        
        private string _bs_name;
        private string _store_type;
        private string _email_to;
        private string _api_version;

        private string XML_FILE_NAME = "settings.xml";
        public ServerCredentials() {
            if (!File.Exists(XML_FILE_NAME)) {
                XDocument doc = new XDocument(new XElement("Settings",
                        new XElement("Servers",
                            new XElement(NodeName))));
                doc.Save(XML_FILE_NAME);
                Console.Write("XML File Created");
            } else {
                XDocument doc = XDocument.Load(XML_FILE_NAME);
                Console.WriteLine(doc.Descendants("Servers").Descendants(NodeName).Count());
                if (doc.Descendants("Servers").Descendants(NodeName).Count() == 0) {
                    doc.Descendants("Servers").FirstOrDefault().Add(new XElement(NodeName));
                    doc.Save(XML_FILE_NAME);
                    Console.Write($"XML File Updated: {0}", NodeName);
                }
            }
        }

        private string get_string(string param, bool encrypted = false)
        {
            XDocument doc = XDocument.Load(XML_FILE_NAME);
            string xml_data = (string)doc.Descendants(NodeName).Descendants(param).FirstOrDefault();
            if (xml_data != null)
                return encrypted? StringUtil.Decrypt(xml_data) : xml_data;
            else
                throw new Exception($"Could not load {NodeName} {param}");
        }
        private void set_string(string value, string param, bool encrypted = false)
        {
            value = encrypted ? StringUtil.Crypt(value) : value;
            XDocument doc = XDocument.Load(XML_FILE_NAME);
            XElement xml_data = doc.Descendants(NodeName).Descendants(param).FirstOrDefault();
            if (xml_data == null)
                doc.Descendants(NodeName).First().Add(new XElement(param, value));
            else
                xml_data.Value = value;
            doc.Save(XML_FILE_NAME);
        }
        public string get_db_conn()
        {
            if (_db_conn != null)
                return _db_conn;
            return get_string("db_conn");
        }
        public void set_db_conn(string value)
        {
            set_string(value, "db_conn");
            _db_conn = value;
        }
        public string get_bs_name()
        {
            if (_bs_name != null)
                return _bs_name;
            return get_string("bs_name");
        }
        public void set_bs_name(string value)
        {
            set_string(value, "bs_name");
            _bs_name = value;
        }
        public string get_store_type()
        {
            if (_store_type != null)
                return _store_type;
            return get_string("store_type");
        }
        public void set_store_type(string value)
        {
            set_string(value, "store_type");
            _store_type = value;
        }

        public string get_email_to() {
            if (_email_to != null)
                return _email_to;
            try {
                return get_string("email_to");
            } catch (Exception) { return null; }
        }
        public void set_email_to(string value) {
            set_string(value, "email_to");
            _email_to = value;
        }
        public string get_api_version()
        {
            if (_api_version != null)
                return _api_version;
            return get_string("api_version");
        }
        public void set_api_version(string value)
        {
            set_string(value, "api_version");
            _api_version = value;
        }

        public string get_store_url() {
            if (_base_url != null)
                return _base_url;
            return get_string("store_base_url");
        }
        public void set_store_url(string value) {
            set_string(value, "store_base_url");
            _base_url = value;
        }

        public string get_pharma_name()
        {
            if (_pharma_name != null)
                return _pharma_name;
            return get_string("pharma_name");
        }
        public void set_pharma_name(string value)
        {
            set_string(value, "pharma_name");
            _pharma_name = value;
        }

        public string get_api_key()
        {
            if (_api_key != null)
                return _api_key; 
            return get_string("api_key", true);
        }
        public void set_api_key(string value)
        {
            set_string(value, "api_key", true);
            _api_key = value;
        }

        public string get_api_username() {
            if (_api_username != null)
                return _api_username;
            return get_string("api_username", true);
        }
        public void set_api_username(string value) {
            set_string(value, "api_username", true);
            _api_username = value;
        }
    }
}
