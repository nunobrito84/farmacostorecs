﻿using FarmacoStore.Dialog;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Models.Store.Opencart;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Utils.Csv;

namespace FarmacoStore.Models.Seeds {
    class SeedsManager {
        //Singleton
        private static readonly SeedsManager instance = new SeedsManager();
        // Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
        static SeedsManager() { }
        private SeedsManager() {
        }
        public static SeedsManager Instance {
            get {
                return instance;
            }
        }

        public ObservableCollection<ICategory> loadSeedCategories(string storeType, BillingSystem.Interfaces.BillingSystemBase.logFunc log) {
            string fileName;
            string hostName = new Uri(new ServerCredentials().get_store_url()).Host.Split('.')[0];
            string[] resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            if (resourceNames.Contains($"FarmacoStore.Assets.Data.Categories.categories_tree_{hostName}.csv"))
                fileName = $"FarmacoStore.Assets.Data.Categories.categories_tree_{hostName}.csv";
            else
                fileName = "FarmacoStore.Assets.Data.Categories.categories_tree.csv";

            log($"Loading seed categories for hostName: {hostName} | Filename: {fileName}");
            using (var reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(fileName))) {
                int cnt = 0;
                ObservableCollection<ICategory> seedCategories = new ObservableCollection<ICategory>();
                var headerDiscard = reader.ReadLine();
                while (!reader.EndOfStream) {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    int id = Int32.Parse(values[0]);
                    int parent_id = Int32.Parse(values[1]);
                    string name = values[2];
                    if (parent_id == 0) {
                        seedCategories.Add(StoreFactory.StoreCategory(storeType, id, parent_id, name, new ObservableCollection<ICategory>()));
                        cnt++;
                    } else {
                        ICategory foundCat = CategoryBase.findCategory(seedCategories, parent_id);
                        if (foundCat != null) {
                            foundCat.children.Add(StoreFactory.StoreCategory(storeType, id, parent_id, name, new ObservableCollection<ICategory>()));
                            cnt++;
                        } else
                            log($"Category seed: Cannot find parent category for id:{id} | parent_id:{parent_id} | name:{name}");
                    }
                }
                log($"Loaded {cnt} items from seed");
                return seedCategories;
            }
        } 

        public List<ProductCategory> loadProductsCategoriesSeed(BillingSystem.Interfaces.BillingSystemBase.logFunc log, ProductsUpdateDlg windowHandler = null) {
            List<ProductCategory> productsCategories = new List<ProductCategory>();

            string fileName;
            string hostName = new Uri(new ServerCredentials().get_store_url()).Host.Split('.')[0];
            string[] resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            if (resourceNames.Contains($"FarmacoStore.Assets.Data.ProductsCategoriesMaps.products_categories_{hostName}.csv"))
                fileName = $"FarmacoStore.Assets.Data.ProductsCategoriesMaps.products_categories_{hostName}.csv";
            else
                fileName = "FarmacoStore.Assets.Data.ProductsCategoriesMaps.products_categories_tree.csv";

            log($"Loading seed products-categories map for hostName: {hostName} | Filename: {fileName}");

            try {
//#if (DEBUG)
                // await loadImages(log, windowHandler);
//                using (CsvReader csv = new CsvReader(new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("FarmacoStore.Assets.Data.ProductsCategoriesMaps.products_categories_debug.csv")), true)) {
//#else
                using (CsvReader csv = new CsvReader(new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(fileName)), true)) {
//#endif
                    int fieldCount = csv.FieldCount;
                    string[] headers = csv.GetFieldHeaders();
                    while (csv.ReadNextRecord()) {
                        int sku = Int32.Parse(csv[0]);
                        int category_id = Int32.Parse(csv[1]);
                        productsCategories.Add(new ProductCategory(sku, category_id));
                        //Debug.WriteLine($"sku {sku}");
                    }
                }
                log($"Loaded {productsCategories.Count} items from products-categories seed");
                productsCategories = productsCategories.OrderBy(o => o.sku).ToList();
            } catch (Exception e) {
                log("Error loadProductsCategories: " + e.Message, true);
            }
            return productsCategories;
        }


        public List<ProductBrand> loadSeedProductsBrands(BillingSystem.Interfaces.BillingSystemBase.logFunc log, ProductsUpdateDlg windowHandler = null) {
            List<ProductBrand> productsBrands = new List<ProductBrand>();
            log("Loading seed brands map");
            try {
                using (CsvReader csv = new CsvReader(new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("FarmacoStore.Assets.Data.Brands.products_brands.csv")), true)) {
                    int fieldCount = csv.FieldCount;
                    string[] headers = csv.GetFieldHeaders();
                    while (csv.ReadNextRecord()) {
                        int sku = Int32.Parse(csv[0]);
                        int brandId = Int32.Parse(csv[1]);
                        productsBrands.Add(new ProductBrand(sku, brandId));
                        //Debug.WriteLine($"sku {sku}");
                    }
                } 
                log($"Loaded {productsBrands.Count} items from products-brands seed");
                //brands = brands.OrderBy(o => o.sku).ToList();
            } catch (Exception e) {
                log("Error loadProductsBrands: " + e.Message, true);
            } 
            return productsBrands;
        }
    }
    public class ProductCategory {
        public int sku { get; }
        public int category_id { get; }
        public ProductCategory(int sku, int category_id) {
            this.sku = sku;
            this.category_id = category_id;
        }
    }
    public class ProductBrand {
        public int sku { get; }
        public int brand_id { get; }
        public ProductBrand(int sku, int brand_id) {
            this.sku = sku; 
            this.brand_id = brand_id;
        }
    }
}
