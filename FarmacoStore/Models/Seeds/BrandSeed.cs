﻿using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FarmacoStore.Models.Seeds {
    public class BrandSeed : BrandBase, IBrand {
        public int web_id {
            get {
                return brand_id - id_web_offset;
            }
        }
        public Image image { get; set; }
        public string imageSrc { get; set; }
        public BrandSeed(int id, string name, Image image) : base(id, name)
        {
            this.image = image;
        }
        public override string ToString() {
            return "[+] " + name;
        }
    }

    public class BrandSeedList {
        public List<BrandSeed> brands = new List<BrandSeed>();

        //Singleton
        private static readonly BrandSeedList instance = new BrandSeedList();
        // Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
        static BrandSeedList() { }
        private BrandSeedList() {
            //Load data here
            Console.WriteLine("Brand Seed exec");
            var executingAssembly = Assembly.GetExecutingAssembly();
            string folderName = string.Format("{0}.Resources.Folder", executingAssembly.GetName().Name);
            string resourcePath = "FarmacoStore.Assets.Brands.";
            string[] filesNames = executingAssembly.GetManifestResourceNames()
                .Where(r => r.StartsWith(resourcePath) && (r.ToLower().EndsWith(".png") || r.ToLower().EndsWith(".jpg")))
                .Select(r => r.Substring(resourcePath.Length))
                .ToArray();
            foreach(string fileName in filesNames) {
                int index = fileName.LastIndexOf('.');
                string onlyName = fileName.Substring(0, index);
                string fileExtension = fileName.Substring(index + 1);
                int brandId = Int32.Parse(onlyName.Split('-')[0]);
                if(brandId > 0) {
                    string brandName = onlyName.Split(new[] { '-' }, 2)[1];
                    Image image = Image.FromStream(System.Reflection.Assembly.GetEntryAssembly().GetManifestResourceStream($"{resourcePath}{onlyName}.{fileExtension}"));
                    brands.Add(new BrandSeed(brandId, brandName, image));
                    Console.WriteLine($"Brand Seed {brandId} - {brandName} available");
                }
            }
        }
        public BitmapImage Convert(Image img) {
            if (img == null)
                return null;
            using (var memory = new MemoryStream()) {
                img.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }
        public static BrandSeedList Instance {
            get {
                return instance;
            }
        }
    }
}
