﻿using FarmacoStore.Models.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;

namespace Utils.ListVirtualization {
    /// <summary>
    /// Demo implementation of IItemsProvider returning dummy customer items after
    /// a pause to simulate network/disk latency.
    /// </summary>
    public class ProductProvider : IItemsProvider<IDbProduct>
    {
        private readonly List<IDbProduct> productsList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductProvider"/> class.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="fetchDelay">The fetch delay.</param>
        public ProductProvider(List<IDbProduct> productsList)
        {
            this.productsList = productsList;
        }

        /// <summary>
        /// Fetches the total number of items available.
        /// </summary>
        /// <returns></returns>
        public int FetchCount()
        {
            Trace.WriteLine("FetchCount");
            return productsList.Count; 
        }

        /// <summary>
        /// Fetches a range of items.
        /// </summary>
        /// <param name="startIndex">The start index.</param>
        /// <param name="count">The number of items to fetch.</param>
        /// <returns></returns>
        public IList<IDbProduct> FetchRange(int startIndex, int count)
        {
            Trace.WriteLine("FetchRange: "+startIndex+","+count);
            if (count > productsList.Count)
                count = productsList.Count;
            else if (startIndex + count > productsList.Count)
                startIndex = productsList.Count - count;
            return productsList.GetRange(startIndex, count);
        }
    }
}
