﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Utils {
    class ObjectUtils {
        public static bool hasProperty(object objectToCheck, string name) {
            var type = objectToCheck.GetType();
            return type.GetMethod(name) != null;
        }
    }
}
