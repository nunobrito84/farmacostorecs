﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FarmacoStore.Utils {
    class HttpFileUpload {
        private string _apiKey;
        private readonly string _boundary = "";

        public HttpFileUpload(string apiKey) {
            _boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
            _apiKey = apiKey;
        }

        public byte[] Upload(string url, MemoryStream dataStream) {
            return Upload(url, dataStream, new NameValueCollection());
        }

        public byte[] Upload(string url, MemoryStream dataStream, NameValueCollection nameValues) {
            HttpWebRequest request = GetHttpWebRequest(url);
            WriteAuthenticationToRequest(request);

            dataStream.Position = 0;
            using (MemoryStream outputStream = new MemoryStream()) {
                WriteNameValuesToStream(outputStream, nameValues);

                WriteDataContentToStream(outputStream, dataStream);

                WriteToHttpStream(request, outputStream);
            }

            // return the response
            return GetHttpWebResponse(request);
        }

        private byte[] GetHttpWebResponse(HttpWebRequest request) {
            using (var response = request.GetResponse()) {
                using (var responseStream = response.GetResponseStream()) {
                    using (var stream = new MemoryStream()) {
                        responseStream.CopyTo(stream);
                        return stream.ToArray();
                    }
                }
            }
        }

        private HttpWebRequest GetHttpWebRequest(string url) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = string.Format("multipart/form-data; boundary={0}", _boundary);
            request.Method = "POST";

            return request;
        }

        private void WriteAuthenticationToRequest(HttpWebRequest request) {
            request.Headers["X-Oc-Restadmin-Id"] = _apiKey;
        }

        private void WriteEndBoundaryToStream(MemoryStream stream) {
            WriteBoundaryToStream(stream, "--");
        }

        private void WriteBoundaryToStream(MemoryStream stream, string endDeliminator) {
            WriteToStream(stream, Encoding.ASCII, string.Format("--{0}{1}", _boundary, endDeliminator));
        }

        private void WriteDataContentToStream(MemoryStream outputStream, MemoryStream inputStream) {
            // write content boundary start
            WriteBoundaryToStream(outputStream, Environment.NewLine);

            string formName = "uploaded";
            string fileName = "file.jpg";
            string contentType = "application/zip";

            WriteToStream(outputStream, System.Text.Encoding.UTF8, string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", formName, fileName, Environment.NewLine));
            WriteToStream(outputStream, System.Text.Encoding.UTF8, string.Format("Content-Type: {0}{1}{1}", contentType, Environment.NewLine));

            byte[] buffer = new byte[inputStream.Length];
            int bytesRead = 0;

            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0) {
                outputStream.Write(buffer, 0, bytesRead);
            }

            // must include a new line before writing the end boundary
            WriteToStream(outputStream, Encoding.ASCII, Environment.NewLine);

            // make sure we end boundary now as the content is finished
            WriteEndBoundaryToStream(outputStream);
        }

        private void WriteNameValuesToStream(MemoryStream stream, NameValueCollection nameValues) {
            foreach (string name in nameValues.Keys) {
                WriteBoundaryToStream(stream, Environment.NewLine);

                WriteToStream(stream, Encoding.UTF8, string.Format("Content-Disposition: form-data; name=\"{0}\"{1}{1}", name, Environment.NewLine));
                WriteToStream(stream, Encoding.UTF8, nameValues[name] + Environment.NewLine);
            }
        }

        private void WriteToHttpStream(HttpWebRequest request, MemoryStream outputStream) {
            request.ContentLength = outputStream.Length;

            using (Stream requestStream = request.GetRequestStream()) {
                outputStream.Position = 0;

                byte[] tempBuffer = new byte[outputStream.Length];
                outputStream.Read(tempBuffer, 0, tempBuffer.Length);
                outputStream.Close();

                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
            }
        }

        private void WriteToStream(MemoryStream stream, Encoding encoding, string output) {
            byte[] headerbytes = encoding.GetBytes(output);
            stream.Write(headerbytes, 0, headerbytes.Length);
        }
    }
}
