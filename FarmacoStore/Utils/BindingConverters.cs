﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FarmacoStore.Utils
{
    public class NotConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }
    public class BooleanAndConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            foreach (object value in values)
            {
                if ((value is bool) && (bool)value == false)
                    return false;
            }
            return true;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("BooleanAndConverter is a OneWay converter.");
        }
    }
    public class HasAnyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value > 0;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("IsMoreThanConverter is a OneWay converter.");
        }
    }
    public class BooleanToVisibilityConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
    public class ReverseBooleanToVisibilityConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            var b = (bool)value;
            if (b) {
                return Visibility.Collapsed;
            } else {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return false;
        }
    }
    /*
    [ValueConversion(typeof(ListView),  typeof(int))]
    public class WidthConverter : IValueConverter {
        public WidthConverter() {
        }
        private ListView listView = null;
        private int strechableColumnIndex = 0;
        public int StrechableColumnIndex {
            get {
                return strechableColumnIndex;
            }
            set {
                strechableColumnIndex = value;
            }
        }
        public ListView OwnedListView {
            get {
                return listView;
            }
            set {
                listView = value;
            }
        }
        public object Convert(object o, Type type, object parameter, CultureInfo culture) {
            GridView gView = listView.View as GridView;
            double total = 0;
            for (int i = 0; i < gView.Columns.Count - 1; i++) {
                total += gView.Columns[i].Width;
            }
            gView.Columns[strechableColumnIndex].Width = listView.ActualWidth - total;
            return listView;
        }
        public object ConvertBack(object o, Type type, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }*/
    /*
    public class WidthConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            int columnsCount = System.Convert.ToInt32(parameter);
            double width = (double)value;
            return width / columnsCount;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }*/
}
