﻿using AutoUpdaterDotNET;
using FarmacoStore.Controller;
using FarmacoStore.Dialog;
using FarmacoStore.Models;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace FarmacoStore
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// https://api.opencart-api.com/demo/admin/#/
    /// 
    public partial class App : System.Windows.Application
    {
        public static string windows_log_entry = "Farmaco Store";
        private NotifyIcon notifyIcon = null;
        private ServerCredentials serverCredentials = new ServerCredentials();

        public App() : base()
        {
            // Setup Quick Converter.
            // Add the System namespace so we can use primitive types (i.e. int, etc.).
            QuickConverter.EquationTokenizer.AddNamespace(typeof(object));
            // Add the System.Windows namespace so we can use Visibility.Collapsed, etc.
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Visibility));
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
           // decimal a = decimal.Parse("4.10");

            EventLog.WriteEntry(App.windows_log_entry, "Application Start", EventLogEntryType.SuccessAudit);

            notifyIcon = new NotifyIcon();
            notifyIcon.MouseClick += new MouseEventHandler(onTrayIconMouseClick);

            notifyIcon.Icon = FarmacoStore.Properties.Resources.storeIcon; 

            MenuItem menuItem1 = new MenuItem();
            MenuItem menuItem2 = new MenuItem();
            MenuItem menuItem3 = new MenuItem();
            MenuItem separatorItem = new MenuItem();

            // Initialize menuItem1
            menuItem1.Index = 0;
            menuItem1.Text = "Fechar aplicação";
            menuItem1.Click += new System.EventHandler(onMenuExit);

            menuItem2.Index = 1;
            menuItem2.Text = "Abrir";
            menuItem2.Click += new System.EventHandler(showProductsDlg);

            menuItem3.Index = 2;
            menuItem3.Text = "Configurações";
            menuItem3.Click += new System.EventHandler(onConfigs);

            separatorItem.Text = "-";
            // Initialize contextMenu1
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.AddRange(new MenuItem[] { menuItem3, separatorItem, menuItem2, separatorItem, menuItem1 });
            notifyIcon.ContextMenu = contextMenu;
            notifyIcon.Text = "Farmaco Store";
            notifyIcon.Visible = true;

            //#if (!DEBUG)
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
            AutoUpdater.Start("http://store.farmacoserver.com/farmacoStore/1.0/updateDescriptor.xml");
            //#else
            //#endif

            String baseUrl, apiKey, pharmaName, dbConn, bsName, storeType, apiVersion;
            try
            {
                baseUrl = serverCredentials.get_store_url();
                apiKey = serverCredentials.get_api_key();
                pharmaName = serverCredentials.get_pharma_name();
                dbConn = serverCredentials.get_db_conn();
                storeType = serverCredentials.get_store_type();
                apiVersion = serverCredentials.get_api_version();
                bsName = serverCredentials.get_bs_name();
            }
            catch
            {
                onConfigs(null, null);
                return;
            }
            UpdateProcessor.Instance.enable();//Start auto-timer
            try {
                IStore store = StoreFactory.Instance(storeType, baseUrl, apiKey);
                ObservableCollection<ICategory> categories = store.getCategories();
                if(categories.Count == 0)
                    showCategoriesCreateDlg(null, null);
                showProductsDlg(null, null);
            }
            catch (Exception error)
            {
                EventLog.WriteEntry(windows_log_entry, error.Message, EventLogEntryType.Error);
            }
        }
        CredentialsDlg bookieConfig;
        private void onConfigs(object sender, EventArgs e)
        {
            if (bookieConfig != null && bookieConfig.Visible)
                bookieConfig.Close();
            bookieConfig = new CredentialsDlg(serverCredentials);
            bookieConfig.Show();
        }
        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args != null)
            {
                if (args.IsUpdateAvailable)
                {
                    MessageBoxResult dialogResult;
                    if (args.Mandatory.Value)
                    {
                        dialogResult =
                            System.Windows.MessageBox.Show(
                                $@"A nova versão {args.CurrentVersion} encontra-se disponível. Está a usar a versão {args.InstalledVersion}. Faça OK para atualizar. Esta atualização é obrigatória.", @"Farmaco Store | Atualização disponível",
                                MessageBoxButton.OK,
                                MessageBoxImage.Information);
                    }
                    else
                    {
                        dialogResult =
                            System.Windows.MessageBox.Show(
                                $@"A nova versão {args.CurrentVersion} encontra-se disponível. Está a usar a versão {
                                        args.InstalledVersion
                                    }. Quer atualizar agora?", @"Farmaco Store | Atualização disponível",
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Information);
                    }

                    // Uncomment the following line if you want to show standard update dialog instead.
                    //AutoUpdater.ShowUpdateForm(args);

                    if (dialogResult.Equals(MessageBoxResult.Yes) || dialogResult.Equals(MessageBoxResult.OK))
                    {
                        try
                        {
                            if (AutoUpdater.DownloadUpdate(args))
                            {
                                System.Windows.Application.Current.Shutdown();
                            }
                        }
                        catch (Exception exception)
                        {
                            System.Windows.MessageBox.Show(exception.Message, exception.GetType().ToString(),
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    //System.Windows.MessageBox.Show(@"Não existe nenhuma atualização disponível.", @"No update available", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                System.Windows.MessageBox.Show(
                        @"Há um problema na ligação ao servidor. Teste a sua ligação da internet e tente novamente mais tarde.",
                        @"Update check failed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void onTrayIconMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                showProductsDlg(null, null);
            //Do the awesome left clickness
        }
        private void onMenuExit(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        ProductsUpdateDlg productsWindow;
        private void showProductsDlg(object sender, EventArgs e)
        {
            UpdateProcessor.Instance.resetAutoTimer();
            if (productsWindow != null && productsWindow.IsVisible)
                productsWindow.Close();
            productsWindow = new ProductsUpdateDlg();
            productsWindow.Show();
        }
        CategoriesCreateDlg categoriesCreateWindow;
        private void showCategoriesCreateDlg(object sender, EventArgs e)
        {
            if (categoriesCreateWindow != null && categoriesCreateWindow.IsVisible)
                categoriesCreateWindow.Close();
            categoriesCreateWindow = new CategoriesCreateDlg();
            categoriesCreateWindow.ShowDialog();
            Console.WriteLine("Categories closed");
        }

    }
}
