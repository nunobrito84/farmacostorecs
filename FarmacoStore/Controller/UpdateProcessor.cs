﻿using FarmacoStore.Dialog;
using FarmacoStore.Models;
using FarmacoStore.Models.BillingSystem;
using FarmacoStore.Models.BillingSystem.Interfaces;
using FarmacoStore.Models.Interfaces;
using FarmacoStore.Models.Seeds;
using FarmacoStore.Models.Store;
using FarmacoStore.Models.Store.Base;
using FarmacoStore.Models.Store.Interfaces;
using FarmacoStore.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Data;
using Utils.Csv;
using Utils.ListVirtualization;
using static FarmacoStore.Models.BillingSystem.Interfaces.BillingSystemBase;

namespace FarmacoStore.Controller
{

    class UpdateProcessor : INotifyPropertyChanged {
        private string _storeType, _storeUrl, _storeAPI, _bsName, _dbConn, _emailTo, _storeName;
        private IStore _store;
        private bool _processing, _processingComplete;
        private string _loading_text = "";
        private bool _enabled = false;
        public ObservableCollection<ICategory> storeCategories;
        public ObservableCollection<IBrand> storeBrands;
        public ObservableCollection<CampaignBase> storeCampaigns;

        public System.Timers.Timer _autoTimer = new System.Timers.Timer(1000);


        public void enable() {
            _enabled = true;
            _autoTimer.Start();
            //Start timer for auto-update here!
        }

        private const int AUTO_TIMER_COUNT_SECONDS = 6*60*60;
        private int AUTO_TIME_PERIODE_SEC = Debugger.IsAttached ? 60 : AUTO_TIMER_COUNT_SECONDS;
        private int _timerCount = Debugger.IsAttached ? 60 : AUTO_TIMER_COUNT_SECONDS;
        public String remainingAutoTimer {
            get {
                var span = new TimeSpan(0, 0, _timerCount); //Or TimeSpan.FromSeconds(seconds); (see Jakob C´s answer)
                return string.Format("{0:00}:{1:00}:{2:00}", (int)span.TotalHours, (int)span.Minutes, span.Seconds);
            }
        }
        public void resetAutoTimer() { _timerCount = AUTO_TIME_PERIODE_SEC; }

        public async void onAutoTimerTick(object source, ElapsedEventArgs e) {
            if (_processing)
                return;

            _timerCount = _timerCount - 1;
            if (_timerCount <= 0) {
                //process here
                Console.Write(" Process! ");
                //restart timer
                _timerCount = AUTO_TIME_PERIODE_SEC;
                logStr = "";//Clear log

                await Task.Run(async () => {
                    bool succeed = true;
                    try {
                        succeed = await UpdateProcessor.Instance.process(false, writeLog, null);
                        writeLog(" Finished Processing with success? " + succeed.ToString());
                    }
                    catch(Exception exception) {
                        writeLog($"Error: {exception.Message}");
                        succeed = false;
                    }
                    //Try to send email 
                    try {
                        if (!string.IsNullOrEmpty(_emailTo)) {
                            //Send email
                            var mailMessage = new MailMessage {
                                From = new MailAddress("webstore@farmaco.pt"),
                                Subject = $"[{(succeed ? "Ok" : "Erro")}] Relatório de atualização da loja online da {_storeName}",
                                Body = $"<h1>Relatório de atualização da webstore</h1><p>{logStr.Replace(Environment.NewLine, "<br/>")}</p>",
                                IsBodyHtml = true,
                            };
                            mailMessage.To.Add(_emailTo);

                            var smtpClient = new SmtpClient("farmaco.pt") {
                                Port = 587,
                                Credentials = new NetworkCredential("webstore@farmaco.pt", "U^F}gGvmZ]ZK"),
                                EnableSsl = true,
                            };
                            smtpClient.Send(mailMessage);
                        }
                    } catch (Exception exc) {
                        EventLog.WriteEntry(App.windows_log_entry, $"An error occured trying to send an email: {exc.Message}", EventLogEntryType.Error);
                    }
                });
            }
            PropertyChanged(this, new PropertyChangedEventArgs("remainingAutoTimer"));
        }

        String logStr = "";
        void writeLog(String str, bool isError = false) {
            const int STR_MAX_LEN = 100000;
            if (logStr.Length > STR_MAX_LEN)
                logStr = "-- CUTTED -- " + Environment.NewLine + logStr.Substring(STR_MAX_LEN / 2);
            else
                logStr += $"[{DateTime.Now.ToString("HH:mm:ss")}]: {str}" + Environment.NewLine;
            EventLog.WriteEntry(App.windows_log_entry, str, EventLogEntryType.SuccessAudit);
        }

        public bool processing {
            get { return this._processing; }
            set {
                if (this._processing != value) {
                    this._processing = value;
                    this.NotifyPropertyChanged("processing");
                }
            }
        }

        public bool processingComplete {
            get { return _processingComplete; }
            set {
                if (this._processingComplete != value) {
                    this._processingComplete = value;
                    this.NotifyPropertyChanged("processingComplete");
                }
            }
        }

        public string loading_text {
            get { return this._loading_text; }
            set {
                if (this._loading_text != value) {
                    this._loading_text = value;
                    this.NotifyPropertyChanged("loading_text");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName) {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public int productsListSelectedCount {
            get {
                //return (_productsList != null) ? _productsList.Count : 0;
                return (_productsList != null) ? _productsList.Count(p => p.IsSelected) : 0;
            }
        }

        private ObservableCollection<IDbProduct> _productsList;
        public ObservableCollection<IDbProduct> productsList {
            get { return this._productsList; }
            set {
                if (this._productsList != value) {
                    if (this._productsList != null) {
                        this._productsList.CollectionChanged -= listOfObjectsCollectionChanged;
                        foreach (IDbProduct item in this._productsList)
                            item.PropertyChanged -= onObjectPropertyChanged;
                    }
                    this._productsList = value;
                    if (this._productsList != null) {
                        this._productsList.CollectionChanged += listOfObjectsCollectionChanged;
                        foreach (IDbProduct item in this._productsList)
                            item.PropertyChanged += onObjectPropertyChanged;
                    }
                    this.NotifyPropertyChanged("productsList");
                    this.NotifyPropertyChanged("productsListSelectedCount");
                }
            }
        }
        void listOfObjectsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            if (e.Action == NotifyCollectionChangedAction.Remove) {
                // When items are removed, unsubscribe from property change notifications.
                var oldItems = (e.OldItems ?? new INotifyPropertyChanged[0]).OfType<INotifyPropertyChanged>();
                foreach (IDbProduct item in oldItems)
                    item.PropertyChanged -= onObjectPropertyChanged;
            }

            // When item(s) are added, subscribe to property notifications.
            if (e.Action == NotifyCollectionChangedAction.Add) {
                var newItems = (e.NewItems ?? new INotifyPropertyChanged[0]).OfType<INotifyPropertyChanged>();
                foreach (IDbProduct item in newItems)
                    item.PropertyChanged += onObjectPropertyChanged;
            }

            // NOTE: I'm not handling NotifyCollectionChangedAction.Reset.
            // You'll want to look into when this event is raised and handle it
            // in a special fashion.
        }
        private void onObjectPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "IsSelected") {
                this.NotifyPropertyChanged("productsListSelectedCount");
            }
        }

        //Singleton 
        private static readonly UpdateProcessor instance = new UpdateProcessor();
        // Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
        static UpdateProcessor() { }
        private UpdateProcessor() {
            ServerCredentials serverCredentials = new ServerCredentials();
            _dbConn = serverCredentials.get_db_conn();
            _bsName = serverCredentials.get_bs_name();
            _storeType = serverCredentials.get_store_type();//To be used several times
            _storeUrl = serverCredentials.get_store_url();
            _storeAPI = serverCredentials.get_api_key();
            _emailTo = serverCredentials.get_email_to();
            _storeName = serverCredentials.get_pharma_name();
            _store = StoreFactory.Instance(_storeType, _storeUrl, _storeAPI);
            _autoTimer.Elapsed += new ElapsedEventHandler(onAutoTimerTick);
            _autoTimer.AutoReset = true;//Repeat all the time
        }
        public static UpdateProcessor Instance {
            get {
                return instance;
            }
        }
        public async void loadCategories() {
            this.storeCategories = await Task.Run(() => _store.getCategories(true));
            this.NotifyPropertyChanged("storeCategories");
        }
        public async void loadBrands() {
            this.storeBrands = await Task.Run(() => _store.getBrands());
            this.NotifyPropertyChanged("storeBrands");
        }

        public async Task<bool> process(bool isManualExecution, logFunc log, ProductsUpdateDlg windowHandler = null) {
            if (!_enabled)
                throw new Exception("Update Processor is not enabled. Are there any missing configs??");

            if (_processing)
                throw new Exception("Update Processor already processing. You must cancel and wait for it to finish.");

            processingComplete = false;

            if (productsList != null && productsList.Count > 0) { //Clear for reload purposes
                //foreach(IDbProduct prod in productsList)
                //    if(prod.ImageFile)
                productsList = new ObservableCollection<IDbProduct>();
            }

            processing = true;

            try {
                loading_text = $"A carregar produtos do {_bsName}";
                IBillingSystem billingSystem = BillingSystemFactory.Instance(_bsName, _dbConn);
                List<IDbProduct> products = billingSystem.getProducts(log);
                log($"[{_bsName}] {products.Count} products loaded.");
                if (products.Count == 0)
                    throw new Exception($"Não existem produtos no {_bsName}... processo cancelado.");

                loading_text = $"A carregar categorias de {_storeUrl}";
                loadCategories();

                loading_text = $"A carregar catálogo de {_storeUrl}";
                loadBrands();

                loading_text = $"A carregar produtos de {_storeUrl}";
                ObservableCollection<IProduct> _storeProducts = _store.getProducts(log);
                log($"[Webstore] {_storeProducts.Count} products loaded from {_storeUrl}");

                //Now we have to merge both lists. When we have a match, attribute the webstore item to the IDbProduct.webstoreProduct property
                //merge products
                int index = 0, newProductsCount = 0, alreadyExistentCount = 0, unknown = 0;
                foreach (IProduct storeProd in _storeProducts) { //Assume the lists are already ordered by product id
                    while (index < products.Count) {
                        if (((IDbProduct)products[index]).productcode == storeProd.sku) {
                            ((IDbProduct)products[index]).webstoreProduct = storeProd;
                            break;
                        } else if (storeProd.sku > ((IDbProduct)products[index]).productcode)
                            index++;
                        else {
                            unknown++;
                            break;
                        };
                    }
                }
                log($"[Webstore] New: {newProductsCount} | Existent: {alreadyExistentCount} | Unknown: {unknown}");
                if (isManualExecution) {

                    loading_text = $"A associar produtos-categorias";
                    List<ProductCategory>productsCategories;
                    productsCategories = await Task.Run(() => SeedsManager.Instance.loadProductsCategoriesSeed(log, windowHandler));
                    //merge products-categories
                    index = 0;
                    unknown = 0;
                    int matchProdCategory = 0;
                    if (productsCategories.Count > 0) {
                        foreach (IDbProduct dbProd in products) { //Assume the lists are already ordered by sku
                            while (index < productsCategories.Count) {
                                if (productsCategories[index].sku == dbProd.productcode) {
                                    matchProdCategory++;
                                    ICategory category = storeCategories.FirstOrDefault(o => o.category_id == productsCategories[index].category_id);
                                    if(category == null) { //This category was not found in the store... disable selection and paint it!
                                        dbProd.IsSelectionEnabled = false;
                                        dbProd.ItemBackColor = System.Windows.Media.Brushes.Plum;
                                        dbProd.categories.Add(new FarmacoStore.Models.Store.Opencart.Category(productsCategories[index].category_id, 0, $"Categoria #{productsCategories[index].category_id} não encontrada na loja", null));
                                    }
                                    else
                                        dbProd.categories.Add(new FarmacoStore.Models.Store.Opencart.Category(productsCategories[index].category_id, 0, category.name, null));
                                    break;
                                } else if (dbProd.productcode > productsCategories[index].sku)
                                    index++;
                                else {
                                    unknown++;
                                    break;
                                };
                            }
                        }
                        log($"[Products-categories] Match: {matchProdCategory} | Unknown: {unknown}");
                    }

                    //Load ProductsBrands 
                    loading_text = $"A associar produtos-marcas";
                    List<ProductBrand> productsBrands;
                    productsBrands = await Task.Run(() => SeedsManager.Instance.loadSeedProductsBrands(log, windowHandler));
                    //merge products-brands
                    index = 0;
                    unknown = 0;
                    int matchProdBrand = 0;
                    if (productsBrands.Count > 0) {
                        List<BrandSeed> seedBrands = BrandSeedList.Instance.brands;
                        foreach (IDbProduct dbProd in products) { //Assume the lists are already ordered by sku
                            while (index < productsBrands.Count) {
                                if (productsBrands[index].sku == dbProd.productcode) {
                                    IBrand storeBrand = storeBrands.FirstOrDefault(o => o.brand_id == productsBrands[index].brand_id);
                                    if (storeBrand != null) {
                                        dbProd.brand = storeBrand;
                                        matchProdBrand++;
                                    } else {
                                        IBrand seedBrand = seedBrands.FirstOrDefault(o => o.brand_id == productsBrands[index].brand_id);
                                        if (seedBrand != null) //This brand was found in the brands seeds
                                            dbProd.brand = seedBrand;
                                        matchProdBrand++;
                                    }
                                    //Attrib brand to product if it is still not associated
                                    if (dbProd.webstoreProduct != null && dbProd.brand != null && dbProd.webstoreProduct.manufacturer_id == 0)
                                        dbProd.productUpdateData.manufacturer_id = dbProd.brand.web_id.ToString();
                                    break;
                                } else if (dbProd.productcode > productsBrands[index].sku)
                                    index++;
                                else {
                                    unknown++;
                                    break;
                                };
                            }
                        }
                        log($"[Products-categories] Match: {matchProdBrand} | Unknown: {unknown}");
                    }

                }
                loading_text = $"A analisar diferenças";
                foreach (IDbProduct prod in products) {
                    if (prod.ProductDataMismatch)//If different, mark to update
                        prod.IsSelected = true;
                    else if (prod.IsInWebStore)
                        prod.IsSelectionEnabled = prod.IsSelected = false;
                    else if (prod.categories.Count == 0) //Products without category cannot be inserted automatically
                        prod.IsSelectionEnabled = prod.IsSelected = false;
                    //Lets count...
                    if (prod.IsInWebStore)
                        newProductsCount++;
                    else
                        alreadyExistentCount++;
                }

                log($"Items to update: {(from x in products where x.IsSelected select x).Count()}");
                loading_text = $"A mostrar resultados";
                productsList = new ObservableCollection<IDbProduct>(products);//Apply list to avoid unnecessary binding events

                if (!isManualExecution)
                    await update(isManualExecution, log, windowHandler);
                Console.Write("finished");
#if (DEBUG)
                // await loadImages(log, windowHandler);
#endif
            } catch (Exception e) {
                log($"Erro: {e.Message}", true);
                return false;
            }
            finally {
                processing = false;
            }
            return true;
        }
        public async Task update(bool isManualExecution, logFunc log, ProductsUpdateDlg windowHandler = null) {
            foreach (IDbProduct prod in productsList) {
                if (prod.IsSelected) {
                    try {
                        if(prod.brand is BrandSeed) { // This is to insert in DB
                            _store.insertBrand(prod.brand as BrandSeed);
                            //Lets assume the brand is inserted... all other products brand must be converted to real brand
                            IBrand newBrand = StoreFactory.StoreBrand(_storeType, prod.brand.brand_id, prod.brand.name, 0, "");
                            storeBrands.Add(newBrand);
                            foreach (IDbProduct prodToClearBrand in productsList)
                                if ((prodToClearBrand.brand is BrandSeed) && prodToClearBrand.brand.brand_id == prod.brand.brand_id)
                                    prodToClearBrand.brand = newBrand;
                        }

                        if (!prod.IsInWebStore || prod.ProductDataMismatch || prod.categories.Count > 0)
                             _store.upsertProduct(prod);
                        if (prod.HasNewImage) {
                            _store.insertProductImage(prod);
                            prod.ImageFile = null;// To remove icon
                        }
                        if (prod.IsInWebStore)
                            prod.ItemBackColor = System.Windows.Media.Brushes.LightYellow;
                        else
                            prod.ItemBackColor = System.Windows.Media.Brushes.LightGreen;
                        OnItemFocus(prod);
                        prod.IsSelected = false;

                        if (!isManualExecution)
                            log($"Updated: {prod.productcode} {prod.name}");
                    } catch (Exception e) {
                        prod.ItemBackColor = System.Windows.Media.Brushes.LightSalmon;
                        prod.ProductTooltip = $"[Webstore] Error upserting product {prod.productcode}: {e.Message}";
                        log(prod.ProductTooltip);
                    } 
                    if (productsList.IndexOf(prod) <= productsList.Count - 1) // this is not the last item
                        Thread.Sleep(100);
                }
            }
            processingComplete = true;
        }
        public async Task loadImages(logFunc log, bool forceImageReplace, ProductsUpdateDlg windowHandler = null) {
            processing = true;
            loading_text = "A procurar novas imagens...";
            int max = 0, count = 0;
            //Get max for info
            foreach (IDbProduct p in productsList) {
                if (p.IsInWebStore && (!p.HasImage || forceImageReplace))
                    max++;
            }
            foreach (IDbProduct prod in productsList) {
                if (prod.IsInWebStore && (!prod.HasImage || forceImageReplace)) {
                    count++;
                    Console.WriteLine($"Finding image #{prod.productcode}");
                    //log($"Finding image #{prod.productcode}");
                    OnItemFocus(prod);
                    using (WebClient webClient = new WebClient()) {
                        try {
                            byte[] imageData = await webClient.DownloadDataTaskAsync(new Uri($"https://store.farmacoserver.com/800x800/{prod.productcode}.jpg"));
                            MemoryStream ms = new MemoryStream(imageData);
                            prod.ImageFile = Image.FromStream(ms);
                            log($"Image 800x800 download for {prod.productcode} found ({count}/{max})");
                            prod.IsSelected = true;
                        } catch (Exception e) {
                            try {
                                //Failed 800x800 ... try 288x390
                                byte[] imageData = await webClient.DownloadDataTaskAsync(new Uri($"https://store.farmacoserver.com/288x390/{prod.productcode}.jpg"));
                                MemoryStream ms = new MemoryStream(imageData);
                                prod.ImageFile = Image.FromStream(ms);
                                log($"Image 288x390 download for {prod.productcode} found ({count}/{max})");
                                prod.IsSelected = true;
                            } catch (Exception e2) {
                                log($"[Error] Image download for {prod.productcode} failed: {e2.Message} ({count}/{max})", true);
                            }
                        }
                    }
                    Thread.Sleep(50);
                }
            }
            processing = false;
        }
        public async Task loadDescriptions(string[] fileNames, logFunc log, ProductsUpdateDlg windowHandler = null) {
            processing = true;
            // Read the files
            foreach (String file in fileNames) {
                try {
                    string fileName = file.Substring(file.LastIndexOf('\\')).Replace("\\", "");
                    loading_text = $"A carregar {fileName}";
                    using (CsvReader csv = new CsvReader(new StreamReader(file), true)) {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        //Get the fields indexes
                        int sku_index = -1;
                        int name_index = -1;
                        int description_index = -1;
                        int meta_desc_index = -1;
                        int meta_keyword_index = -1;
                        int meta_title_index = -1;
                        int tag_index = -1;
                        for (int i = 0; i < headers.Length; i++) {
                            string header = headers[i];
                            switch(header.ToLower()) {
                                case "sku":
                                case "product_code":
                                case "proudctcode":
                                    sku_index = i;
                                    break;
                                case "name":
                                    name_index = i;
                                    break;
                                case "description":
                                    description_index = i;
                                    break;
                                case "short_description":
                                    meta_desc_index = i;
                                    break;
                                case "meta_keyword":
                                    meta_keyword_index = i;
                                    break;
                                case "meta_title":
                                    meta_title_index = i;
                                    break;
                                case "tags":
                                    tag_index = i;
                                    break;
                            }
                        }
                        if (sku_index < 0)
                            throw new Exception("Invalid format. sku key not found.");
                        int nrecords = 0, foundrecords = 0;
                        while (csv.ReadNextRecord()) {
                            nrecords++;
                            int sku = Int32.Parse(csv[sku_index]);
                            IDbProduct product = productsList.FirstOrDefault(o => o.productcode == sku);
                            if(product != null) {
                                foundrecords++;
                                if (name_index >= 0)
                                    product.productUpdateData.name = csv[name_index];
                                if (description_index >= 0)
                                    product.productUpdateData.description = csv[description_index];
                                if (meta_desc_index >= 0)
                                    product.productUpdateData.meta_desc = csv[meta_desc_index];
                                if (meta_keyword_index >= 0)
                                    product.productUpdateData.meta_keyword = csv[meta_keyword_index];
                                if (meta_title_index >= 0)
                                    product.productUpdateData.meta_title = csv[meta_title_index];
                                if (tag_index >= 0)
                                    product.productUpdateData.meta_tag = csv[tag_index];

                                if (product.ProductDataMismatch)  //If different, mark to update
                                    product.IsSelectionEnabled = product.IsSelected = true;
                            }
                        }
                        log($"CSV file: {fileName} match: {foundrecords}/{nrecords}");
                    }
                } catch (SecurityException ex) {
                    // The user lacks appropriate permissions to read files, discover paths, etc.
                    string msg = "Security error. Please contact your administrator for details.\n\n" +
                        "Error message: " + ex.Message + "\n\n" +
                        "Details (send to Support):\n\n" + ex.StackTrace;
                    System.Windows.MessageBox.Show(msg);
                } catch (Exception ex) {
                    // Could not load the image - probably related to Windows file system permissions.
                    string msg = "Error loading: " + file.Substring(file.LastIndexOf('\\'))
                        + ". You may not have permission to read the file, or " +
                        "it may be corrupt.\n\nReported error: " + ex.Message;
                    log(msg, true);
                    System.Windows.MessageBox.Show(msg);
                }
            }
            //log($"Loaded {productsCategories.Count} items from products-categories seed");
            processing = false;
        }

        public event EventHandler<ItemFocusEventArgs> ItemFocus;    // the Event
        public class ItemFocusEventArgs : EventArgs    // guideline: derive from EventArgs
{
            public ItemFocusEventArgs(IDbProduct product) { this.Product = product; }
            public IDbProduct Product { get; set; }
        }
        protected virtual void OnItemFocus(IDbProduct product)    // the Trigger method, called to raise the event
        {
            EventHandler<ItemFocusEventArgs> handler = ItemFocus;
            handler?.Invoke(this, new ItemFocusEventArgs(product));
        }


    }
}
